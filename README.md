# Responsive Image Formatter Tools (RIFT)

RIFT strives to provide a Quality-of-Life improvements while setting up Drupal
Site for serving Responsive Images.

In a nutshell RIFT module provides,

- An interface to define "Responsive Image" (a.k.a RIFT View Mode) that would be implemented using  — srcset/sizes/<picture> — strategy.
- Apply Responsive Image to a Media field (Image) attached to content entity using Field formatters.

RIFT is a collection of Drupal services, Field Formatters and Twig functions
that can be used to fine tune the responsive image handling on the site.

The project is in its early phase and would start by re-using most of the
functionality that already exists in the ecosystem. More details to follow
as we develop.

You can checkout detailed documention [Documentation](https://www.drupal.org/docs/extending-drupal/contributed-modules/contributed-module-documentation/responsive-image-formatter-tools-rift)
which also has a [Quick Start Guide](https://www.drupal.org/docs/extending-drupal/contributed-modules/contributed-module-documentation/responsive-image-formatter-tools-rift/rift-quick-start)
about the project.

## Table of contents

- Requirements
- Recommended modules
- Installation
- Configuration
- Maintainers

## Requirements

RIFT depends on following contrib modules,

- image_widget_crop
- combined_image_style
- image_style_quality
- focal_point
- crop

## Recommended modules

- ace_editor

## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

Before you can configure the module, you would need to understand and figure out
the Responsive Image strategy for your site. You can checkout the [Responsive Image
Design Guide](https://www.drupal.org/docs/extending-drupal/contributed-modules/contributed-module-documentation/responsive-image-formatter-tools-rift/responsive-image-design-guide) and various resources on the internet for this purpose.

For configuring RIFT Module, please refer to following documenation,
- [Configuration Guide for Site Builders](https://www.drupal.org/docs/extending-drupal/contributed-modules/contributed-module-documentation/responsive-image-formatter-tools-rift/responsive-image-design-guide/site-builder)
- [Configuration Reference](https://www.drupal.org/docs/extending-drupal/contributed-modules/contributed-module-documentation/responsive-image-formatter-tools-rift/rift-configuration-reference)

## Maintainers

- Shibin Das - [D34dMan](https://www.drupal.org/u/d34dman)
