import './app.css'
import App from './App.svelte'

const app = new App({
  target: document.getElementById('rift-ui')!,
})

export default app
