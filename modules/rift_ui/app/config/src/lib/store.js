import { writable, derived } from 'svelte/store';
export let aspect_ratios = writable([]);
export let viewModes = writable([]);
export let screens = writable([]);
export let formats = writable([]);
export let multipliers = writable([]);
export let attributes = writable([]);
export let isSyncing = writable(false);
export let isScreensDirty = writable(false);
export const defaultEmptyConfig = {
	source: "image",
	media_source: "combined_image_style",
	aspect_ratios: [],
	config: {
		screens: {},
		transforms: "",
		multipliers: [],
		quality: {},
		formats: [],
		attributes: {},
		fallback_transform: ""
	},
	view_modes: {}
};

export let drupalSettings = writable({
	app_config: {
		formats: {},
		media_source: {},
		source: {},
	},
	aspect_ratios: [],
	source: "image",
	media_source: "combined_image_style",
	config: {
		screens: {},
		transforms: "",
		multipliers: [],
		quality: {},
		formats: [],
		attributes: {},
		fallback_transform: ""
	},
	view_modes: []
});

function transformViewModeSizes(screenValues, viewMode) {
	let temAspectRatios = viewMode.aspect_ratios
		.split(/[\s]+/);
	let tempSizes = new Object();
	viewMode.sizes
		.split(/[\s]+/)
		.map((val, ndx, arr) => {
			let values = val.split(':');
			tempSizes[values[0]] = {
				width: values[1],
				aspect_ratio: temAspectRatios[ndx]
			};
		});
	let output = [];
	output = screenValues.map((v, index, arr) => {
		return {
			id: index,
			screen_name: v.name,
			screen_width: v.width,
			enabled: !!tempSizes[v.name],
			width: tempSizes[v.name]?.width,
			aspect_ratio: tempSizes[v.name]?.aspect_ratio,
		};
	});
	return output;
}

function transformAttributes(attributes) {
	let attributes_items = [];
	let id = 0;
	for (const [key, value] of Object.entries(attributes)) {
		attributes_items.push({
			id: id,
			name: key,
			value
		});
		id++;
	}
	return attributes_items;
}

export const transformViewModesToStore = function (screenValues, viewModeConfig) {
	let viewModesItems = [];
	for (const [key, value] of Object.entries(viewModeConfig)) {
		viewModesItems.push({
			id: key,
			key: key,
			sizes: transformViewModeSizes(screenValues, value),
			label: value.label,
			attributes: transformAttributes(value?.attributes ?? {}),
			fallback_transform: value.fallback_transform,
		});
	}
	viewModes.set(viewModesItems);
}

export const transformAspectRatiosToStore = function (aspect_ratio_config) {
	aspect_ratios.set(aspect_ratio_config.map((value, ndx) => {
		return {
			id: ndx,
			value: value,
		}
	}));
}

export const transformConfigToStore = function (config) {
	// Initialize "formats" store.
	multipliers.set(config.multipliers.map((value, ndx) => {
		return {
			id: ndx,
			multiplier: value,
			quality: config.quality[value] ?? '',
		};
	}));

	formats.set(config.formats.map((value, ndx) => {
		return {
			id: ndx,
			format: value,
		}
	}))

	let screen_items = [];
	let id = 0;
	for (const [key, value] of Object.entries(config.screens)) {
		screen_items.push({
			id: id,
			name: key,
			...value
		});
		id++;
	}
	screens.set(screen_items);
	attributes.set(transformAttributes(config.attributes));

}