import daisyui from "daisyui"

/** @type {import('tailwindcss').Config} */
export default {
  content: [
    './src/**/*.{svelte,js,ts}',
  ],
  safelist: [
    {
      pattern: /grid-cols-+/,
    },
    {
      pattern: /col-span-+/,
    },
  ],
  theme: {
    extend: {},
  },
  daisyui: {
    themes: [
      "corporate"
    ],
  },
  plugins: [
    daisyui,
  ],
}

