<?php

declare(strict_types=1);

namespace Drupal\rift_ui\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Responsive Image Formatter tools UI routes.
 */
final class RiftUiMediaController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function __invoke(): array {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => '<div id="rift-ui"></div>',
      '#attached' => [
        'library' => [
          'rift_ui/admin',
        ],
      ],
    ];

    return $build;
  }

}
