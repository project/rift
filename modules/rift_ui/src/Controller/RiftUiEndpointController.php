<?php

declare(strict_types=1);

namespace Drupal\rift_ui\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\media\MediaInterface;
use Drupal\rift\RiftPicture;
use Drupal\rift\RiftSettings;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for Responsive Image Formatter tools UI routes.
 */
final class RiftUiEndpointController implements ContainerInjectionInterface {

  /**
   * The controller constructor.
   */
  public function __construct(
    private readonly RiftPicture $riftPicture,
    private readonly RendererInterface $renderer,
    private readonly RiftSettings $riftSettings,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('rift.picture'),
      $container->get('renderer'),
      $container->get('rift.settings'),
    );
  }

  /**
   * Builds the response.
   */
  public function getMedia(MediaInterface $media) {
    $settings = $this->riftSettings->getSettings();
    $config = $settings['config'];
    $view_modes = $settings['view_modes'];
    $output = [];
    foreach ($view_modes as $key => $view_mode) {
      $runtime_config = $config;
      foreach ($view_mode as $k => $v) {
        $runtime_config[$k] = $v;
      }
      $build = $this->riftPicture->responsivePicture($media, $runtime_config);
      $output_string = $this->renderer->render($build);
      $output['view_modes'][$key] = [
        'html' => $output_string,
      ];
    }
    return new JsonResponse($output);
  }

  /**
   * Settings Read.
   */
  public function getSettings() {
    $output = $this->riftSettings->getSettings();
    $output['app_config'] = [
      'source' => $this->riftSettings->getAvailableSourcePlugins(),
      'media_source' => $this->riftSettings->getAvailableMediaSourcePlugins(),
      'formats' => $this->riftSettings->getAvailableExtensions(),
    ];
    return new JsonResponse($output);
  }

  /**
   * Settings write.
   */
  public function setSettings(Request $request) {
    $content = $request->getContent();
    $decoded = Json::decode($content);
    $this->riftSettings->saveSettings($decoded);
    return $this->getSettings();
  }

}
