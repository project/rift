<?php

declare(strict_types=1);

namespace Drupal\rift_ui\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Responsive Image Formatter tools UI routes.
 */
final class RiftUiConfigController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function buildConfig(): array {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => '<div id="rift-config"></div>',
      '#attached' => [
        'library' => [
          'rift_ui/config',
        ],
      ],
    ];

    return $build;
  }

}
