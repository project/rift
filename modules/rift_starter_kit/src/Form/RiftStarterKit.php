<?php

declare(strict_types=1);

namespace Drupal\rift_starter_kit\Form;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\ImageToolkit\ImageToolkitManager;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Provides a RIFT Starter Kit form.
 */
final class RiftStarterKit extends FormBase {

  /**
   * Template Directory.
   *
   * String.
   */
  protected string $templatePath;

  public function __construct(
    private readonly ImageToolkitManager $imageToolkitManager,
    private readonly UuidInterface $uuid,
    private readonly ModuleExtensionList $extensionList,
    protected $configFactory,
  ) {
    $this->templatePath = $this->extensionList->getPath('rift_starter_kit') . '/assets/templates/';
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      $container->get('image.toolkit.manager'),
      $container->get('uuid'),
      $container->get('extension.list.module'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'rift_starter_kit_rift_starter_kit';
  }

  /**
   * Get available extension suggestions.
   *
   * @return string[]
   *   List of extension.
   */
  protected function getAvailableExtensions(): array {
    $extensions = $this->imageToolkitManager->getDefaultToolkit()->getSupportedExtensions();
    $options = array_combine(
      $extensions,
      array_map('mb_strtoupper', $extensions)
    );
    return $options;
  }

  /**
   * Get default aspect ratio suggestions.
   *
   * @return string[]
   *   List of aspect ratio.
   */
  protected function getDefaultAspectRatios(): array {
    return [
      '1x1',
      '1x2',
      '1x3',
      '3x1',
      '3x4',
      '4x1',
      '4x3',
      '9x16',
      '12x7',
      '16x9',
    ];
  }

  /**
   * Get default extension suggestions.
   *
   * @return string[]
   *   List of extension.
   */
  protected function getDefaultExtensions(): array {
    return [
      'avif',
      'webp',
      'jpeg',
    ];
  }

  /**
   * Get default quality suggestions.
   *
   * @return string[]
   *   List of quality.
   */
  protected function getDefaultQuality(): array {
    return [
      '90',
      '80',
      '70',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $default_aspect_ratios = $this->getDefaultAspectRatios();
    $form['aspect_ratios'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Aspect Ratios'),
      '#default_value' => implode("\n", $default_aspect_ratios),
      '#rows' => count($default_aspect_ratios) + 2,
    ];
    $form['extensions'] = [
      '#type' => 'checkboxes',
      '#options' => $this->getAvailableExtensions(),
      '#title' => $this->t('File extensions'),
      '#default_value' => $this->getDefaultExtensions(),
    ];
    $form['quality'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Image Quality'),
      '#default_value' => implode("\n", $this->getDefaultQuality()),
    ];

    $url = Url::fromRoute('rift.settings');
    $rift_setting_link = Link::fromTextAndUrl($this->t('RIFT Settings'), $url);
    $form['generate_rift_settings'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Generate RIFT settings'),
      '#default_value' => TRUE,
      '#description' => $this->t('WARNING! Enabling this setting will replace current @link.', ['@link' => $rift_setting_link->toString(TRUE)]),
    ];

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Generate'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    if (!empty($aspect_ratios = $this->convertMultilineTextToArray($form_state->getValue('aspect_ratios')))) {
      foreach ($aspect_ratios as $aspect_ratio) {
        if (empty($aspect_ratio)) {
          continue;
        }
        [$w, $h] = explode('x', $aspect_ratio);
        if (!is_numeric($w)) {
          $form_state->setErrorByName('aspect_ratios', $this->t('Width must be an integer, invalid width value in @value.', ['@value' => $aspect_ratio]));
        }
        if (!is_numeric($h)) {
          $form_state->setErrorByName('aspect_ratios', $this->t('Height must be an integer, invalid height value in @value.', ['@value' => $aspect_ratio]));
        }
      }
    }
    if (!empty($quality_items = $this->convertMultilineTextToArray($form_state->getValue('quality')))) {
      foreach ($quality_items as $quality_item) {
        if (empty($quality_item)) {
          continue;
        }
        if (!is_numeric($quality_item)) {
          $form_state->setErrorByName('quality', $this->t('Quality must be an integer, invalid value @value.', ['@value' => $quality_item]));
        }
      }
    }
  }

  /**
   * Convert a multiline text to array.
   *
   * @param string $text
   *   Text to be converted to array.
   *
   * @return string[]
   *   Each new line in text as a item in an array.
   */
  protected function convertMultilineTextToArray(string $text): array {
    $text = str_replace(["\r\n", "\r"], "\n", $text);
    $items = explode("\n", $text);
    return array_filter($items);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    if (empty($aspect_ratios = $this->convertMultilineTextToArray($form_state->getValue('aspect_ratios')))) {
      $aspect_ratios = $this->getDefaultAspectRatios();
    }
    $this->generateAspectRatiosConfigurations($aspect_ratios);
    if (empty($quality_items = $this->convertMultilineTextToArray($form_state->getValue('quality')))) {
      $quality_items = $this->getDefaultAspectRatios();
    }
    $this->generateQualityConfigurations($quality_items);
    $extensions = $form_state->getValue('extensions');
    $extensions = array_filter($extensions);
    if (empty($extensions)) {
      $extensions = $this->getDefaultExtensions();
    }
    $this->generateExtensionConfigurations($extensions);

    if ($form_state->getValue('generate_rift_settings')) {
      $this->generateConfig('rift.settings.yml', 'rift.settings', [], TRUE);
    }

    $this->generateConfig('image.style.rift_fallback.yml', 'image.style.rift_fallback', [
      '%width%' => '400',
      '%height%' => '300',
    ]);
    $this->generateConfig('image.style.nop.yml', 'image.style.nop', []);
    $this->generateConfig('crop.type.focal_point.yml', 'crop.type.focal_point', []);
  }

  /**
   * Generate extension configurations.
   *
   * @param array $extensions
   *   Array of extensions to be generated.
   */
  protected function generateExtensionConfigurations($extensions) {
    foreach ($extensions as $extension) {
      $context = [
        '%extension%' => $extension,
      ];
      $this->generateConfig('image.style.image_convert.yml', 'image.style.' . $extension, $context);
    }
  }

  /**
   * Generate quality configurations.
   *
   * @param string[] $quality_items
   *   Array of quality configuration to be generated.
   */
  protected function generateQualityConfigurations($quality_items) {
    foreach ($quality_items as $quality) {
      $context = [
        '%quality%' => $quality,
      ];
      $this->generateConfig('image.style.image_style_quality.yml', 'image.style.' . $quality, $context);
    }
  }

  /**
   * Generate aspect ratio configurations.
   *
   * @param string[] $aspect_ratios
   *   Array of aspect ratios to be generated.
   */
  protected function generateAspectRatiosConfigurations($aspect_ratios) {
    foreach ($aspect_ratios as $aspect_ratio) {
      $context = [
        '%aspect_ratio%' => $aspect_ratio,
      ];
      $this->generateConfig('crop.type.aspect_ratio.yml', 'crop.type.' . $aspect_ratio, $context);
      $this->generateConfig('image.style.crop_crop.yml', 'image.style.' . $aspect_ratio, $context);
    }
  }

  /**
   * Generate configuration from template.
   *
   * @param string $template
   *   The template to load for generating configuration.
   * @param string $config_id
   *   The config id.
   * @param array $context
   *   Variables to be used for replacing the values in the template.
   *   The variable key must be of the format "%variable_name%".
   * @param bool $replace_existing_config
   *   Controls if existing configuration should be replaced or not.
   */
  protected function generateConfig(string $template, string $config_id, array $context, bool $replace_existing_config = FALSE) {
    $config_path = $this->templatePath . $template;
    $content = file_get_contents($config_path);
    for ($ndx = 1; $ndx <= 10; $ndx++) {
      $context['%uuid_' . $ndx . '%'] = $this->uuid->generate();
    }
    $transformed_content = str_replace(array_keys($context), $context, $content);
    $data = Yaml::parse($transformed_content);
    $config = $this
      ->configFactory
      ->getEditable($config_id)
      ->setData($data);
    if ($config->isNew()) {
      $config
        ->save(TRUE);
      $this->messenger()->addStatus($this->t('Generated configuration @config_id.', ['@config_id' => $config_id]));
    }
    else {
      if ($replace_existing_config) {
        $config
          ->save(TRUE);
        $this->messenger()->addStatus($this->t('Updated configuration @config_id.', ['@config_id' => $config_id]));
      }
      else {
        $this->messenger()->addWarning($this->t('Skipped generation of configuration for @config_id, since it is already present in the system.', ['@config_id' => $config_id]));
      }
    }
  }

}
