<?php

namespace Drupal\rift\Routing;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Route Event Subscriber.
 */
class RouteSubscriber extends RouteSubscriberBase {

  public function __construct(
    private ModuleHandlerInterface $moduleHandler,
  ) {}

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection): void {
    /** @var \Symfony\Component\Routing\Route $route */
    if ($route = $collection->get('image.style_public')) {
      if ($this->moduleHandler->moduleExists('combined_image_style')) {
        return;
      }
      $path = $route->getPath();

      $route->setPath(str_replace('image_style', 'image_styles', $path));
      $route->setDefault('_controller', '\Drupal\rift\Controller\ImageStyleDownloadController::deliverCombined');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -1024];
    return $events;
  }

}
