<?php

namespace Drupal\rift;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Plugin manager for "rift_media_source".
 */
class RiftMediaSourceManager extends DefaultPluginManager {

  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler,
  ) {
    parent::__construct(
      'Plugin/RiftMediaSource',
      $namespaces,
      $module_handler,
      'Drupal\rift\RiftMediaSourceInterface',
      'Drupal\rift\Attribute\RiftMediaSource'
    );
    $this->alterInfo($this->getType());
    $this->setCacheBackend($cache_backend, 'rift_media_source_plugins');
  }

  /**
   * {@inheritdoc}
   */
  protected function getType() {
    return 'rift_media_source';
  }

}
