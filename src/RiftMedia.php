<?php

namespace Drupal\rift;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\crop\Entity\Crop;
use Drupal\file\FileInterface;
use Drupal\media\MediaInterface;

/**
 * Service description.
 *
 * @deprecated in rift:1.0.2 and is removed from rift:2.0.0.
 * This service is obsolete and hence would be removed. The functionality of
 * the service has been taken over by the RiftMediaSource Plugin.
 * @see https://www.drupal.org/project/rift/issues/3470088
 */
class RiftMedia {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The crop storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $cropTypeStorage;

  /**
   * Constructs a RiftMedia object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->cropTypeStorage = $this->entityTypeManager->getStorage('crop_type');
  }

  /**
   * Detect if a manual crop is applied on the image.
   */
  public function detectIfCropExists(MediaInterface $media, $aspect_ratio_style): bool {
    $crop_type = $this->cropTypeStorage->load($aspect_ratio_style);
    if (empty($crop_type)) {
      return FALSE;
    }
    $field_name = $this->getMediaFieldName($media);
    $file = $media->get($field_name)->entity;
    if ($file instanceof FileInterface) {
      $file_uri = $file->getFileUri();
      return Crop::cropExists($file_uri, $aspect_ratio_style);
    }
    return FALSE;
  }

  /**
   * Detect Media field name.
   */
  public function getMediaFieldName(MediaInterface $media): string {
    $source = $media->getSource();
    return $source->getConfiguration()["source_field"];
  }

}
