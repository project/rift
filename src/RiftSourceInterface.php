<?php

namespace Drupal\rift;

/**
 * Interface to be used by RiftSource Plugins.
 */
interface RiftSourceInterface {

  /**
   * Generate Image url based on style definition.
   *
   * @param array $input_styles
   *   List of styles / transforms to perform on the image.
   * @param string|null $uri
   *   Original image url.
   *
   * @return string
   *   The image url.
   */
  public function generate(array $input_styles, ?string $uri): string;

}
