<?php

namespace Drupal\rift\Html;

/**
 * The <picture> HTML Element.
 */
class PictureElement extends ElementBase {

  /**
   * The '<source>' elements.
   *
   * @var \Drupal\rift\Html\SourceElement[]
   */
  protected array $source;

  /**
   * The '<img>' element.
   *
   * @var \Drupal\rift\Html\ImgElement
   */
  protected ?ImgElement $img;

  /**
   * {@inheritDoc}
   */
  public function getInnerHtmlElements(): array {
    $elements = [];
    foreach ($this->source as $source) {
      $elements[] = $source->render();
    }
    if ($this->img) {
      $elements[] = $this->img->render();
    }
    return $elements;
  }

  /**
   * {@inheritDoc}
   */
  public function getTagName(): string {
    return 'picture';
  }

  /**
   * Getter for Source.
   *
   * @return \Drupal\rift\Html\SourceElement[]
   *   return Source.
   */
  public function getSource(): array {
    return $this->source;
  }

  /**
   * Setter for Source.
   *
   * @param \Drupal\rift\Html\SourceElement[] $source
   *   Source value.
   *
   * @return PictureElement
   *   Self Reference.
   */
  public function setSource(array $source): PictureElement {
    $this->source = $source;
    return $this;
  }

  /**
   * Getter for Img.
   *
   * @return \Drupal\rift\Html\ImgElement
   *   return Img.
   */
  public function getImg(): ?ImgElement {
    return $this->img;
  }

  /**
   * Setter for Img.
   *
   * @param \Drupal\rift\Html\ImgElement $img
   *   Img value.
   *
   * @return PictureElement
   *   Self Reference.
   */
  public function setImg(?ImgElement $img): PictureElement {
    $this->img = $img;
    return $this;
  }

}
