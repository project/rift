<?php

namespace Drupal\rift\Html;

/**
 * The 'sizes' attribute.
 */
class SizesItem {

  /**
   * The media query part of the sizes attribute.
   *
   * @var string
   */
  protected string $mediaQuery;

  /**
   * The width part of the sizes attribute.
   *
   * @var string
   */
  protected string $width;

  /**
   * Getter for MediaQuery.
   *
   * @return string
   *   return MediaQuery.
   */
  public function getMediaQuery(): string {
    return $this->mediaQuery;
  }

  /**
   * Setter for MediaQuery.
   *
   * @param string $mediaQuery
   *   MediaQuery value.
   *
   * @return SizesItem
   *   Self Reference.
   */
  public function setMediaQuery(string $mediaQuery): SizesItem {
    $this->mediaQuery = $mediaQuery;
    return $this;
  }

  /**
   * Getter for Width.
   *
   * @return string
   *   return Width.
   */
  public function getWidth(): string {
    return $this->width;
  }

  /**
   * Setter for Width.
   *
   * @param string $width
   *   Width value.
   *
   * @return SizesItem
   *   Self Reference.
   */
  public function setWidth(string $width): SizesItem {
    $this->width = $width;
    return $this;
  }

  /**
   * Get the string equivalent for "sizes" attribute.
   */
  public function toString(): string {
    $items = [];
    if ($this->getMediaQuery()) {
      $items[] = $this->getMediaQuery();
    }
    if ($this->getWidth()) {
      $items[] = $this->getWidth();
    }
    return implode(' ', $items);
  }

}
