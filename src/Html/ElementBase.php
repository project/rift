<?php

namespace Drupal\rift\Html;

use Drupal\Core\Template\Attribute;

/**
 * Base class for abstracting HTML Elements.
 */
abstract class ElementBase {

  /**
   * Extra attributes that can be added to the element.
   *
   * @var \Drupal\Core\Template\Attribute
   */
  protected Attribute $attribute;

  /**
   * The constructor.
   */
  public function __construct() {
    $this->attribute = new Attribute();
  }

  /**
   * Generates Drupal Render array for printing the Html Element.
   */
  public function render(): array {
    return [
      '#type' => 'inline_template',
      '#template' => '<{{ tag_name }} {{ attributes }}>{{ content|render }}</{{ tag_name }}>',
      '#context' => [
        'tag_name' => $this->getTagName(),
        'content' => $this->getInnerHtmlElements(),
        'attributes' => $this->getAttribute(),
      ],
    ];
  }

  /**
   * Getter for Attribute.
   *
   * @return \Drupal\Core\Template\Attribute
   *   return Attribute.
   */
  public function getAttribute(): Attribute {
    return $this->attribute;
  }

  /**
   * Setter for Attribute.
   *
   * @param \Drupal\Core\Template\Attribute $attribute
   *   Attribute value.
   *
   * @return ElementBase
   *   Self Reference.
   */
  public function setAttribute(Attribute $attribute): ElementBase {
    $this->attribute = $attribute;
    return $this;
  }

  /**
   * Getter for InnerHtmlElements.
   *
   * @return array
   *   return InnerHtmlElements.
   */
  public function getInnerHtmlElements(): array {
    return [];
  }

  /**
   * The tag name of the HTML Element.
   */
  abstract protected function getTagName(): string;

}
