<?php

namespace Drupal\rift\Html;

/**
 * The 'srcset' attribute.
 */
class SrcSetItem {
  /**
   * The image url.
   *
   * @var string
   */
  protected string $img;

  /**
   * The width of the image.
   *
   * @var string
   */
  protected string $width;

  /**
   * Getter for Img.
   *
   * @return string
   *   return Img.
   */
  public function getImg(): string {
    return $this->img;
  }

  /**
   * Setter for Img.
   *
   * @param string $img
   *   Img value.
   *
   * @return SrcSetItem
   *   Self Reference.
   */
  public function setImg(string $img): SrcSetItem {
    $this->img = $img;
    return $this;
  }

  /**
   * Getter for Width.
   *
   * @return string
   *   return Width.
   */
  public function getWidth(): string {
    return $this->width;
  }

  /**
   * Setter for Width.
   *
   * @param string $width
   *   Width value.
   *
   * @return SrcSetItem
   *   Self Reference.
   */
  public function setWidth(string $width): SrcSetItem {
    $this->width = $width;
    return $this;
  }

  /**
   * Get the string equivalent for "srcset" attribute.
   */
  public function toString(): string {
    $items = [];
    if ($this->getImg()) {
      $items[] = $this->getImg();
    }
    if ($this->getWidth()) {
      $items[] = $this->getWidth();
    }
    return implode(' ', $items);
  }

}
