<?php

namespace Drupal\rift\Html;

use Drupal\Core\Template\Attribute;

/**
 * The <img> HTML element.
 */
class ImgElement extends ElementBase {

  /**
   * The 'src' attribute.
   *
   * @var string|null
   */
  protected ?string $src = NULL;

  /**
   * The 'srcset' attribute.
   *
   * @var \Drupal\rift\Html\SrcSetItem[]
   */
  protected array $srcSet = [];

  /**
   * The 'sizes' attribute.
   *
   * @var \Drupal\rift\Html\SizesItem[]
   */
  protected array $sizes = [];

  /**
   * The 'alt' attribute.
   *
   * @var string|null
   */
  protected ?string $alt = NULL;

  /**
   * The 'width' attribute.
   *
   * @var int|null
   */
  protected ?int $width = NULL;

  /**
   * The 'height' attribute.
   *
   * @var int|null
   */
  protected ?int $height = NULL;

  /**
   * The 'title' attribute.
   *
   * @var string|null
   */
  protected ?string $title = NULL;

  /**
   * Generates Drupal Render array for printing the Html Element.
   */
  public function render(): array {
    return [
      '#type' => 'inline_template',
      '#template' => '<{{ tag_name }} {{ attributes }} />',
      '#context' => [
        'tag_name' => $this->getTagName(),
        'attributes' => $this->getAttribute(),
      ],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getAttribute(): Attribute {
    $attribute = parent::getAttribute();
    if (!empty($this->src)) {
      $attribute->setAttribute('src', $this->src);
    }
    if (!empty($this->alt)) {
      $attribute->setAttribute('alt', $this->alt);
    }
    if (!empty($this->title)) {
      $attribute->setAttribute('title', $this->title);
    }
    if (!empty($this->width)) {
      $attribute->setAttribute('width', strval($this->width));
    }
    if (!empty($this->height)) {
      $attribute->setAttribute('height', strval($this->height));
    }
    if (!empty($this->sizes)) {
      $sizes = [];
      foreach ($this->sizes as $sizes_item) {
        $sizes[] = $sizes_item->toString();
      }
      $attribute->setAttribute('sizes', implode(',', $sizes));
    }
    if (!empty($this->srcset)) {
      $srcset = [];
      foreach ($this->srcset as $srcset_item) {
        $srcset[] = $srcset_item->toString();
      }
      $attribute->setAttribute('srcset', implode(',', $srcset));
    }
    return $attribute;
  }

  /**
   * {@inheritDoc}
   */
  public function getTagName(): string {
    return 'img';
  }

  /**
   * Getter for Src.
   *
   * @return string|null
   *   return Src.
   */
  public function getSrc(): ?string {
    return $this->src;
  }

  /**
   * Setter for Src.
   *
   * @param string $src
   *   Src value.
   *
   * @return ImgElement
   *   Self Reference.
   */
  public function setSrc(string $src): ImgElement {
    $this->src = $src;
    return $this;
  }

  /**
   * Getter for SrcSet.
   *
   * @return \Drupal\rift\Html\SrcSetItem[]
   *   return SrcSet.
   */
  public function getSrcSet(): array {
    return $this->srcSet;
  }

  /**
   * Setter for SrcSet.
   *
   * @param \Drupal\rift\Html\SrcSetItem[] $srcSet
   *   SrcSet value.
   *
   * @return ImgElement
   *   Self Reference.
   */
  public function setSrcSet(array $srcSet): ImgElement {
    $this->srcSet = $srcSet;
    return $this;
  }

  /**
   * Getter for Sizes.
   *
   * @return \Drupal\rift\Html\SizesItem[]
   *   return Sizes.
   */
  public function getSizes(): array {
    return $this->sizes;
  }

  /**
   * Setter for Sizes.
   *
   * @param \Drupal\rift\Html\SizesItem[] $sizes
   *   Sizes value.
   *
   * @return ImgElement
   *   Self Reference.
   */
  public function setSizes(array $sizes): ImgElement {
    $this->sizes = $sizes;
    return $this;
  }

  /**
   * Getter for Alt.
   *
   * @return string
   *   return Alt.
   */
  public function getAlt(): string {
    return $this->alt ?? '';
  }

  /**
   * Setter for Alt.
   *
   * @param string|null $alt
   *   Alt value.
   *
   * @return ImgElement
   *   Self Reference.
   */
  public function setAlt(?string $alt): ImgElement {
    $this->alt = $alt;
    return $this;
  }

  /**
   * Getter for Width.
   *
   * @return int|null
   *   return Width.
   */
  public function getWidth(): ?int {
    return $this->width;
  }

  /**
   * Setter for Width.
   *
   * @param int $width
   *   Width value.
   *
   * @return ImgElement
   *   Self Reference.
   */
  public function setWidth(int $width): ImgElement {
    $this->width = $width;
    return $this;
  }

  /**
   * Getter for Height.
   *
   * @return int|null
   *   return Height.
   */
  public function getHeight(): ?int {
    return $this->height;
  }

  /**
   * Setter for Height.
   *
   * @param int $height
   *   Height value.
   *
   * @return ImgElement
   *   Self Reference.
   */
  public function setHeight(int $height): ImgElement {
    $this->height = $height;
    return $this;
  }

  /**
   * Getter for Title.
   *
   * @return string|null
   *   return Title.
   */
  public function getTitle(): ?string {
    return $this->title;
  }

  /**
   * Setter for Title.
   *
   * @param string|null $title
   *   Title value.
   *
   * @return ImgElement
   *   Self Reference.
   */
  public function setTitle(?string $title): ImgElement {
    $this->title = $title;
    return $this;
  }

}
