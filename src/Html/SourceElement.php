<?php

namespace Drupal\rift\Html;

use Drupal\Core\Template\Attribute;

/**
 * The <source> HTML Element.
 */
class SourceElement extends ElementBase {

  /**
   * The 'src' attribute.
   *
   * @var string|null
   */
  protected ?string $src = NULL;

  /**
   * The 'type' attribute.
   *
   * @var string|null
   */
  protected ?string $type = NULL;

  /**
   * The 'media' attribute.
   *
   * @var string|null
   */
  protected ?string $media = NULL;

  /**
   * The 'sizes' attribute.
   *
   * @var \Drupal\rift\Html\SizesItem[]
   */
  protected array $sizes = [];

  /**
   * The 'srcset' attribute.
   *
   * @var \Drupal\rift\Html\SrcSetItem[]
   */
  protected array $srcset = [];

  /**
   * The 'width' attribute.
   *
   * @var int|null
   */
  protected ?int $width = NULL;

  /**
   * The 'height' attribute.
   *
   * @var int|null
   */
  protected ?int $height = NULL;

  /**
   * Generates Drupal Render array for printing the Html Element.
   */
  public function render(): array {
    return [
      '#type' => 'inline_template',
      '#template' => '<{{ tag_name }} {{ attributes }} />',
      '#context' => [
        'tag_name' => $this->getTagName(),
        'attributes' => $this->getAttribute(),
      ],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getAttribute(): Attribute {
    $attribute = parent::getAttribute();
    if (!empty($this->width)) {
      $attribute->setAttribute('width', strval($this->width));
    }
    if (!empty($this->height)) {
      $attribute->setAttribute('height', strval($this->height));
    }
    if (!empty($this->src)) {
      $attribute->setAttribute('src', $this->src);
    }
    if (!empty($this->type)) {
      $attribute->setAttribute('type', $this->type);
    }
    if (!empty($this->media)) {
      $attribute->setAttribute('media', $this->media);
    }
    if (!empty($this->sizes)) {
      $sizes = [];
      foreach ($this->sizes as $sizes_item) {
        $sizes[] = $sizes_item->toString();
      }
      $attribute->setAttribute('sizes', implode(',', $sizes));
    }
    if (!empty($this->srcset)) {
      $srcset = [];
      foreach ($this->srcset as $srcset_item) {
        $srcset[] = $srcset_item->toString();
      }
      $attribute->setAttribute('srcset', implode(',', $srcset));
    }
    return $attribute;
  }

  /**
   * Getter for Src.
   *
   * @return string
   *   return Src.
   */
  public function getSrc(): string {
    return $this->src;
  }

  /**
   * Setter for Src.
   *
   * @param string $src
   *   Src value.
   *
   * @return SourceElement
   *   Self Reference.
   */
  public function setSrc(string $src): SourceElement {
    $this->src = $src;
    return $this;
  }

  /**
   * Getter for Type.
   *
   * @return string
   *   return Type.
   */
  public function getType(): string {
    return $this->type;
  }

  /**
   * Setter for Type.
   *
   * @param string $type
   *   Type value.
   *
   * @return SourceElement
   *   Self Reference.
   */
  public function setType(string $type): SourceElement {
    $this->type = $type;
    return $this;
  }

  /**
   * Getter for Media.
   *
   * @return string
   *   return Media.
   */
  public function getMedia(): string {
    return $this->media;
  }

  /**
   * Setter for Media.
   *
   * @param string $media
   *   Media value.
   *
   * @return SourceElement
   *   Self Reference.
   */
  public function setMedia(string $media): SourceElement {
    $this->media = $media;
    return $this;
  }

  /**
   * Getter for Sizes.
   *
   * @return \Drupal\rift\Html\SizesItem[]
   *   return Sizes.
   */
  public function getSizes(): array {
    return $this->sizes;
  }

  /**
   * Setter for Sizes.
   *
   * @param \Drupal\rift\Html\SizesItem[] $sizes
   *   Sizes value.
   *
   * @return SourceElement
   *   Self Reference.
   */
  public function setSizes(array $sizes): SourceElement {
    $this->sizes = $sizes;
    return $this;
  }

  /**
   * Getter for Srcset.
   *
   * @return \Drupal\rift\Html\SrcSetItem[]
   *   return Srcset.
   */
  public function getSrcset(): array {
    return $this->srcset;
  }

  /**
   * Setter for Srcset.
   *
   * @param \Drupal\rift\Html\SrcSetItem[] $srcset
   *   Srcset value.
   *
   * @return SourceElement
   *   Self Reference.
   */
  public function setSrcset(array $srcset): SourceElement {
    $this->srcset = $srcset;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getTagName(): string {
    return 'source';
  }

  /**
   * Getter for Width.
   *
   * @return int|null
   *   return Width.
   */
  public function getWidth(): ?int {
    return $this->width;
  }

  /**
   * Setter for Width.
   *
   * @param int $width
   *   Width value.
   *
   * @return SourceElement
   *   Self Reference.
   */
  public function setWidth(int $width): SourceElement {
    $this->width = $width;
    return $this;
  }

  /**
   * Getter for Height.
   *
   * @return int|null
   *   return Height.
   */
  public function getHeight(): ?int {
    return $this->height;
  }

  /**
   * Setter for Height.
   *
   * @param int $height
   *   Height value.
   *
   * @return SourceElement
   *   Self Reference.
   */
  public function setHeight(int $height): SourceElement {
    $this->height = $height;
    return $this;
  }

}
