<?php

namespace Drupal\rift;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\RendererInterface;
use Drupal\media\MediaInterface;
use Drupal\rift\DTO\PictureConfig;
use Drupal\rift\DTO\SourceTransformConfig;
use Drupal\rift\Html\ImgElement;
use Drupal\rift\Html\PictureElement;
use Drupal\rift\Html\SizesItem;
use Drupal\rift\Html\SourceElement;
use Drupal\rift\Html\SrcSetItem;
use Drupal\image\Entity\ImageStyle;

/**
 * Service to generate responsive picture markup from a media entity.
 */
class RiftPicture {

  /**
   * Global RIFT Media Source plugin.
   *
   * @var \Drupal\rift\RiftMediaSourceInterface
   */
  protected RiftMediaSourceInterface $mediaSourcePlugin;

  /**
   * Global RIFT Source plugin.
   *
   * @var \Drupal\rift\RiftMediaSourceInterface
   */
  protected RiftSourceInterface $sourcePlugin;

  /**
   * Stores overall configuration.
   *
   * @var \Drupal\rift\PictureConfig
   */
  protected PictureConfig $pictureConfig;

  /**
   * Constructs the TwigTweakExtension object.
   */
  public function __construct(
    private readonly RendererInterface $renderer,
    private readonly ConfigFactoryInterface $configFactory,
    private readonly RiftSourceManager $riftSource,
    private readonly RiftMediaSourceManager $riftMediaSource,
  ) {
    $this->pictureConfig = new PictureConfig();
    $this->setMediaSourcePlugin($this->getDefaultMediaSourcePluginId());
    $this->setSourcePlugin($this->getDefaultSourcePluginId());
  }

  /**
   * Get the default "media source" plugin ID.
   *
   * @return string
   *   The media source plugin id.
   */
  protected function getDefaultMediaSourcePluginId() : string {
    return $this->configFactory->get('rift.settings')->get('media_source') ?: 'image';
  }

  /**
   * Get the default "source" plugin ID.
   *
   * @return string
   *   The source plugin id.
   */
  protected function getDefaultSourcePluginId() : string {
    return $this->configFactory->get('rift.settings')->get('source') ?: 'combined_image_style';
  }

  /**
   * Setter for "source" plugin.
   */
  public function setSourcePlugin(string $source_plugin_id) {
    $this->sourcePlugin = $this->riftSource->createInstance($source_plugin_id);
  }

  /**
   * Setter for "media_source" plugin.
   */
  public function setMediaSourcePlugin(string $media_source_plugin_id) {
    $this->mediaSourcePlugin = $this->riftMediaSource->createInstance($media_source_plugin_id);
  }

  /**
   * Generate picture sources from a given configuration and media entity.
   *
   * @param \Drupal\rift\PictureConfig $config
   *   Picture configuration.
   * @param \Drupal\media\MediaInterface|null $media
   *   The media entity.
   *
   * @return array
   *   Render array for responsive picture.
   */
  protected function generatePictureSources(PictureConfig $config, ?MediaInterface $media = NULL): array {
    $picture_sources_data = [];
    $image_transforms = [];
    $ndx = 0;
    foreach ($config->sizes as $size) {
      $aspect_ratio_style = $config->aspectRatios[$ndx] ?: '100x1';
      [
        $screen,
        $view_width,
        $width,
      ] = $this->extractWidthFromScreenAndViewWidth($size, $config->screens);
      $transform = new SourceTransformConfig(
        size: $size,
        screen: $screen,
        screenWidth: $config->screens[$screen]->width,
        viewWidth:  $view_width,
        width: intval($width),
        mediaQuery: $config->screens[$screen]->mediaQuery,
        effectiveMediaQuery: !isset($config->sizes[$ndx + 1]) ? FALSE : $config->screens[$screen]->mediaQuery,
        aspectRatioStyle:  $aspect_ratio_style,
        transformStyle:  !empty($config->transforms[$ndx]) ? $config->transforms[$ndx] : FALSE,
      );
      $ndx++;
      $image_transforms[] = $transform;
    }

    foreach ($config->formats as $format) {
      foreach ($image_transforms as $transform) {
        $width = $transform->width;
        $srcsets = [];
        foreach ($config->multipliers as $multiplier) {
          $multiplierTransformConfig = clone $transform;
          $quality = $config->quality[$multiplier];
          $m = floatval(trim($multiplier, 'x'));
          $multiplierTransformConfig->width = intval($width * $m);
          $srcset = [
            'styles' => [
              'image_boundaries' => $this->mediaSourcePlugin->generateImageBoundaryStyle($media, $multiplierTransformConfig),
              'main' => $multiplierTransformConfig->transformStyle,
              'format' => $format == 'original' ? FALSE : $format,
              'quality' => $quality ?: FALSE,
            ],
            'multiplier' => $multiplier,
            'type' => $format == 'original' ? FALSE : 'image/' . $format,
            'source_transform_config' => $multiplierTransformConfig,
          ];
          $srcsets[] = $srcset;
        }
        $picture_sources_data[] = [
          'media' => $transform->effectiveMediaQuery,
          'sizes' => $transform->viewWidth,
          'view_width' => $transform->viewWidth,
          'srcset' => $srcsets,
          'width' => $transform->width,
          'source_transform_config' => $transform,
        ];
      }
    }
    return $picture_sources_data;
  }

  /**
   * Get sitewide default configurations.
   *
   * @return array
   *   The default configurations.
   */
  protected function getDefaultConfig(): array {
    $config = $this->configFactory->get('rift.settings')->get('config');
    $config['url_generation_strategy'] = $this->getDefaultSourcePluginId();
    return $config;
  }

  /**
   * Responsive Picture twig filter.
   *
   * @param array|null $element
   *   Render array element.
   * @param array $config
   *   Configuration for generating srcset.
   *
   * @return string[]
   *   Render array for responsive picture tag.
   *
   *  @deprecated in rift:1.3.2 and is removed from rift:2.0.0.
   *  This method was supposed to be used via Twig Filter. However, the
   *  twig filter expects a Media Entity to be provided, instead of
   *  field content. Thus using the twig filter "rift_picture" is not
   *  supported by rift media and this method would be removed.
   *
   *  @see https://www.drupal.org/project/rift/issues/3495399
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function responsivePictureFromElement($element, array $config = []): array {
    // @todo Instead of bubbleUpCacheTags, inject proper cache context.
    // Cache context varies based on RULE and Media entity.
    $this->bubbleUpCacheTags($element);
    $media = $this->getImageMedia($element);
    if ($media instanceof MediaInterface) {
      return $this->responsivePicture($media, $config);
    }
    return [
      '#markup' => '<!-- Missing Image -->',
    ];
  }

  /**
   * Responsive Picture twig filter.
   *
   * @param \Drupal\media\MediaInterface $media
   *   Media entity.
   * @param array $config
   *   Configuration for generating srcset.
   *   <code>
   *    node.field_common_image.0.entity|rift_picture({
   *      sizes: 'xs:100vw sm:100vw md:50vw lg:50vw xl:50vw',
   *      aspect_ratios: "1x1 16x9 1x1 1x1 1x1",
   *      quality: {
   *        "1x": "80",
   *        "2x": "40",
   *      },
   *      url_generation_strategy: "dummyimage",
   *      formats: ['webp', 'avif', 'jpeg'],
   *    })
   *   </code>.
   *
   * @return string[]
   *   Render array for responsive picture tag.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function responsivePicture(?MediaInterface $media = NULL, array $config = []): array {
    $config = array_merge($this->getDefaultConfig(), $config);
    if (empty($config['id'])) {
      $config['id'] = '#' . md5(serialize($config));
    }
    $this->pictureConfig
      ->processScreens($config['screens'])
      ->processSizes($config['sizes'])
      ->processTransforms($config['transforms'])
      ->processMultipliers($config['multipliers'])
      ->processQuality($config['quality'])
      ->processAspectRatios($config['aspect_ratios'])
      ->processFormats($config['formats'])
      ->processAttribute($config['attributes'])
      ->processFallbackTransforms($config['fallback_transform'])
      ->processUrlGenerationStrategy($config['url_generation_strategy']);

    if (!$this->mediaSourcePlugin->validateMedia($media)) {
      return [
        '#markup' => '<!-- Missing Image -->',
      ];
    }

    if (empty($this->pictureConfig->formats)) {
      return [
        '#markup' => '<!-- Missing Formats -->',
      ];
    }

    if (empty($this->pictureConfig->screens)) {
      return [
        '#markup' => '<!-- Missing screens -->',
      ];
    }
    $this->setSourcePlugin($this->pictureConfig->urlGenerationStrategy);
    $sources = $this->generatePictureSources($this->pictureConfig, $media);

    if (empty($sources)) {
      // @todo Check if we can provide placeholder images.
      return [
        '#markup' => '<!-- Missing Image -->',
      ];
    }

    $picture_element = new PictureElement();
    $source_elements = [];
    foreach ($sources as $source) {
      $image_source = $this->mediaSourcePlugin->getImageData($media, $source['source_transform_config']);
      $source_elements[] = $this->generateSourceTagFromStyleArray($source, $image_source->getSrc());
    }
    $picture_element->setSource($source_elements);

    // Generate the <img> tag inside <picture>.
    $image_source = $this->mediaSourcePlugin->getImageData($media);

    $dimensions = $this->getImageDimension($image_source, $this->pictureConfig->fallbackTransform);

    $url = $this
      ->generateImageUrlFromStyles(explode('-', $this->pictureConfig->fallbackTransform), $image_source->getSrc());
    $img_element = new ImgElement();
    $img_element
      ->setSrc($url)
      ->setAlt($image_source->getAlt())
      ->setTitle($image_source->getTitle())
      ->setHeight($dimensions['height'] ?: $image_source->getHeight())
      ->setWidth($dimensions['width'] ?: $image_source->getWidth());

    $picture_element->setImg($img_element);
    $picture_element->setAttribute($this->pictureConfig->attribute);

    // We avoid using dependency injection to avoid circular dependency.
    // phpcs:ignore DrupalPractice.Objects.GlobalDrupal.GlobalDrupal
    if (\Drupal::service('twig')->isDebug()) {
      $output = [
        [
          '#type' => 'inline_template',
          '#template' => '<!-- RIFT View Mode : {{ view_mode }} -->',
          '#context' => [
            'view_mode' => $config['id'],
          ],
        ],
        $picture_element->render(),
      ];
    }
    else {
      $output = $picture_element->render();
    }
    return $output;
  }

  /**
   * Finds height and width of an image.
   *
   * @param \Drupal\rift\Html\ImgElement $image_source
   *   The image source.
   * @param string|null $combined_image_style
   *   The combined image sylte.
   *
   * @return array
   *   Dimension of the image.
   */
  protected function getImageDimension(ImgElement $image_source, ?string $combined_image_style = '') {
    $url = $image_source->getSrc();
    $dimensions = [
      'width' => $image_source->getWidth(),
      'height' => $image_source->getHeight(),
    ];
    foreach (explode('-', $combined_image_style) as $style) {
      // phpcs:ignore DrupalPractice.Objects.GlobalClass.GlobalClass
      $image_style = ImageStyle::load($style);
      if ($image_style) {
        $image_style->transformDimensions($dimensions, $url);
      }
    }
    return $dimensions;
  }

  /**
   * Detect Image file uri from given render element.
   *
   * @param array|null $element
   *   Render array element.
   *
   * @return \Drupal\media\MediaInterface|null
   *   Uri if detected, otherwise null.
   *
   *  @deprecated in rift:1.3.2 and is removed from rift:2.0.0.
   *  This method was supposed to be used via Twig Filter. However, the
   *  twig filter expects a Media Entity to be provided, instead of
   *  field content. Thus using the twig filter "rift_picture" is not
   *  supported by rift media and this method would be removed.
   *
   *  @see https://www.drupal.org/project/rift/issues/3495399
   */
  protected function getImageMedia($element): ?MediaInterface {
    $children = array_intersect_key($element, array_flip(Element::children($element)));
    $media = $children[0]['#media'] ?: NULL;
    if ($media instanceof MediaInterface) {
      $source = $media->getSource();
      if ($source->getPluginId() === 'image') {
        return $media;
      }
    }
    return NULL;
  }

  /**
   * Provide bubbling up of cache tags on render elements when bypassing render.
   *
   * @deprecated in rift:1.3.2 and is removed from rift:2.0.0.
   * This method was supposed to be used via Twig Filter. However, the
   * twig filter expects a Media Entity to be provided, instead of
   * field content. Thus using the twig filter "rift_picture" is not
   * supported by rift media and this method would be removed.
   *
   * @see https://www.drupal.org/project/rift/issues/3495399
   */
  public function bubbleUpCacheTags(&$element) {
    // @todo Find proper way to do this.
    if (is_array($element) && array_key_exists('#cache', $element)) {
      // Early return if this element was pre-rendered (no need to re-render).
      // @todo Check if this case is handled by twig_capture.
      if (isset($element['#printed']) && $element['#printed'] == TRUE && isset($element['#markup']) && strlen($element['#markup']) > 0) {
        return;
      }
      show($element);
      $this->renderer->render($element);
    }
  }

  /**
   * Generate <source> tag from style definitions.
   *
   * @param mixed $config
   *   Configurations from which a source tag can be generated.
   * @param string|null $uri
   *   The uri for source tag.
   *
   * @return \Drupal\rift\Html\SourceElement
   *   Returns source tag render array.
   */
  protected function generateSourceTagFromStyleArray(mixed $config, ?string $uri): SourceElement {
    $srcset_items = [];
    $source_element = new SourceElement();
    foreach ($config['srcset'] as $source) {
      $url = $this->generateImageUrlFromStyles($source['styles'], $uri);
      $srcset_item = new SrcSetItem();
      $srcset_item->setImg($url);
      // Unlike traditional width, the width in srcset item is the multiplier.
      $srcset_item->setWidth($source['multiplier']);
      $srcset_items[] = $srcset_item;
      // We set the type to the last one in the source.
      // Is this good idea?
      if ($source['type']) {
        $source_element->setType($source['type']);
      }
      if (!$source_element->getHeight()) {
        if ($width = $source['source_transform_config']->width) {
          [$arw, $arh] = explode('x', $source['source_transform_config']->aspectRatioStyle);
          if ($arh > 0) {
            $aspect_ratio = intval($arw) / intval($arh);
            $height = intval($width / $aspect_ratio);
            if ($height) {
              $source_element->setWidth($width);
              $source_element->setHeight($height);
            }
          }
        }
      }
    }
    $sizes_item = new SizesItem();
    $sizes_item->setMediaQuery($config['media']);
    $sizes_item->setWidth($config['view_width']);
    $source_element
      ->setMedia($config['media'])
      ->setSizes([$sizes_item])
      ->setSrcset($srcset_items);
    return $source_element;
  }

  /**
   * Helper method to extract width from screen and view width definition.
   *
   * @param string $size
   *   Size definition (E.g. sm:100vw, lg:400px, ...).
   * @param \Drupal\rift\DTO\ScreenConfig[] $screens
   *   Screen configurations.
   *
   * @return array
   *   Returns screen, view_width definition and width.
   *
   * @todo fix return type.
   */
  protected function extractWidthFromScreenAndViewWidth(string $size, array $screens): array {
    [$screen, $view_width] = explode(':', $size);
    $screen_width = $screens[$screen]->width;
    $view_width = trim($view_width, 'px');
    if (str_ends_with($view_width, 'vw')) {
      $percent = floatval(trim($view_width, 'vw'));
      $width = intval($screen_width * $percent / 100);
    }
    else {
      $width = $view_width;
    }
    return [$screen, $view_width, $width];
  }

  /**
   * Generate Image url from image styles definitions.
   */
  protected function generateImageUrlFromStyles(array $input_styles, ?string $uri): string {
    return $this
      ->sourcePlugin
      ->generate($input_styles, $uri);
  }

}
