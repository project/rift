<?php

namespace Drupal\rift\DTO;

/**
 * Picture Configuration DTO.
 */
class SourceConfig {

  /**
   * Extra settings for source.
   *
   * @var array
   */
  public readonly array $settings;

  /**
   * Constructor.
   */
  public function __construct(
    public readonly ?int $height,
    public readonly int $width,
    public readonly string $viewWidth,
    public readonly string $screen,
    public readonly string $aspectRatiosStyle,
    array $multipliers,
    array $quality,
  ) {
    $settings = [];
    foreach ($multipliers as $multiplier) {
      $settings[$multiplier] = [
        'multiplier' => trim($multiplier, 'x'),
        'quality' => $quality[$multiplier],
      ];
    }
    $this->settings = $settings;
  }

}
