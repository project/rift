<?php

namespace Drupal\rift\DTO;

/**
 * DTO for passing transformation configurations around.
 */
class SourceTransformConfig {

  public function __construct(
    public ?string $size = NULL,
    public ?string $screen = NULL,
    public ?int $screenWidth = NULL,
    public ?string $viewWidth = NULL,
    public ?int $width = NULL,
    public ?string $mediaQuery = NULL,
    public ?string $effectiveMediaQuery = NULL,
    public ?string $aspectRatioStyle = NULL,
    public ?string $transformStyle = NULL,
  ) {}

}
