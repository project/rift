<?php

namespace Drupal\rift\DTO;

use Drupal\Core\Template\Attribute;

/**
 * Picture Configuration DTO.
 */
class PictureConfig {
  const INPUT_RULE__DEFAULT = 'default';
  const INPUT_RULE__VIEW_WIDTH = 'vw';
  const INPUT_RULE__PERCENT = 'percent';
  const INPUT_RULE__HEIGHT_WIDTH = 'hxw';

  /**
   * Processed screen configuration.
   *
   * @var \Drupal\rift\DTO\ScreenConfig[]
   */
  public array $screens;

  /**
   * Aspect ratio definitions.
   *
   * @var string[]
   */
  public array $aspectRatios;

  /**
   * Custom transforms definitions.
   *
   * @var string[]
   */
  public array $transforms;

  /**
   * Sizes definitions.
   *
   * @var string[]
   */
  public array $sizes;

  /**
   * Multiplier to be used for each breakpoint to support high density screens.
   *
   * @var string[]
   */
  public array $multipliers;

  /**
   * Quality to be set for each multiplier.
   *
   * @var string[]
   */
  public array $quality;

  /**
   * The source element configurations.
   *
   * @var \Drupal\rift\DTO\SourceConfig[]
   */
  protected array $items;

  /**
   * Quality to be set for each multiplier.
   *
   * @var string[]
   */
  public array $formats;

  /**
   * Custom attributes to be added to the 'img' element.
   *
   * @var \Drupal\Core\Template\Attribute
   */
  public Attribute $attribute;

  /**
   * The Image Style to be used for 'img' element.
   *
   * @var string
   */
  public string $fallbackTransform;

  /**
   * The url generation strategy.
   *
   * @var string
   */
  public string $urlGenerationStrategy;

  /**
   * Get a source config object for a given index.
   *
   * @param int $index
   *   The index to retrieve the source config from.
   *
   * @return \Drupal\rift\DTO\SourceConfig|null
   *   Source config if present.
   */
  public function get(int $index): ?SourceConfig {
    return $this->items[$index] ?? NULL;
  }

  /**
   * Get screen for a given index.
   */
  private function getScreenOfIndex(int|string $ndx): string {
    $size = $this->sizes[$ndx];
    [$screen] = explode(':', $size);
    return $screen;
  }

  /**
   * Get view width for a given index.
   */
  private function getViewWidthOfIndex(int|string $ndx): string {
    $size = $this->sizes[$ndx];
    [, $view_width] = explode(':', $size);
    $view_width = trim($view_width, 'px');
    return $view_width;
  }

  /**
   * Detect input rule in play.
   *
   * @param mixed $view_width
   *   The view width parameter supplied with the rule.
   *
   * @return string
   *   The rule type.
   */
  private function detectRule(mixed $view_width): string {
    if (substr($view_width, -2) == 'vw') {
      return $this::INPUT_RULE__VIEW_WIDTH;
    }
    return $this::INPUT_RULE__DEFAULT;
  }

  /**
   * Process screens.
   */
  public function processScreens(array $screens): PictureConfig {
    $output = [];
    foreach ($screens as $breakpoint => $screen) {
      $output[$breakpoint] = new ScreenConfig($screen['width'], $screen['media']);
    }
    $this->screens = $output;
    return $this;
  }

  /**
   * Process sizes.
   */
  public function processSizes(string $sizes): PictureConfig {
    $this->sizes = explode(' ', $sizes);
    return $this;
  }

  /**
   * Process multipliers.
   */
  public function processMultipliers(array $multipliers): PictureConfig {
    $this->multipliers = $multipliers;
    return $this;
  }

  /**
   * Process quality.
   */
  public function processQuality(array $quality): PictureConfig {
    $q = [];
    foreach ($this->multipliers as $multiplier) {
      $q[$multiplier] = $quality[$multiplier] ?: FALSE;
    }
    $this->quality = $q;
    return $this;
  }

  /**
   * Process transforms.
   */
  public function processTransforms(string $transforms): PictureConfig {
    $this->transforms = explode(' ', $transforms);
    return $this;
  }

  /**
   * Process formats.
   */
  public function processFormats(array $formats): PictureConfig {
    $this->formats = $formats;
    return $this;
  }

  /**
   * Process attributes.
   */
  public function processAttribute(array $attributes): PictureConfig {
    $this->attribute = new Attribute($attributes);
    return $this;
  }

  /**
   * Process fallback transform.
   */
  public function processFallbackTransforms(string $fallback_transform): PictureConfig {
    $this->fallbackTransform = $fallback_transform;
    return $this;
  }

  /**
   * Process aspect ratios.
   */
  public function processAspectRatios($aspect_ratios): PictureConfig {
    $multipliers = $this->multipliers;
    $aspect_ratios_styles = explode(' ', $aspect_ratios);
    $aspect_ratios_style_bag = [];
    foreach ($aspect_ratios_styles as $ndx => $aspect_ratios_style) {
      $screen = $this->getScreenOfIndex($ndx);
      $view_width = $this->getViewWidthOfIndex($ndx);
      $a = explode('-', $aspect_ratios_style);
      $aspect_ratios_style_1 = array_shift($a);
      switch ($this->detectRule($view_width)) {
        case $this::INPUT_RULE__VIEW_WIDTH:
          $view_width_percent = intval(trim($view_width, 'vw'));
          $width = intval($this->screens[$screen]->width * $view_width_percent / 100);
          $source_config = new SourceConfig(
            NULL,
            $width,
            $view_width,
            $screen,
            $aspect_ratios_style,
            $multipliers,
            $this->quality,
          );
          break;

        default:
          $height_style = array_shift($a);
          $width = intval($view_width);
          $height = intval(trim((string) $height_style, 'h'));
          $source_config = new SourceConfig(
            $height,
            $width,
            $view_width,
            $screen,
            $aspect_ratios_style,
            $multipliers,
            $this->quality,
          );
          break;
      }
      $aspect_ratios_style_bag[] = $aspect_ratios_style_1;
      $this->items[$ndx] = $source_config;
    }
    $this->aspectRatios = $aspect_ratios_style_bag;
    return $this;
  }

  /**
   * Process url generation strategy.
   */
  public function processUrlGenerationStrategy(string $strategy): PictureConfig {
    $this->urlGenerationStrategy = $strategy;
    return $this;
  }

}
