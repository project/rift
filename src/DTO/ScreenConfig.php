<?php

namespace Drupal\rift\DTO;

/**
 * Screen Configuration DTO.
 */
class ScreenConfig {

  /**
   * Constructor.
   */
  public function __construct(
    public readonly int $width,
    public readonly string $mediaQuery,
  ) {}

}
