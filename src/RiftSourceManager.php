<?php

namespace Drupal\rift;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Plugin manager for "rift_source".
 */
class RiftSourceManager extends DefaultPluginManager {

  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler,
  ) {
    parent::__construct(
      'Plugin/RiftSource',
      $namespaces,
      $module_handler,
      'Drupal\rift\RiftSourceInterface',
      'Drupal\rift\Attribute\RiftSource'
    );
    $this->alterInfo($this->getType());
    $this->setCacheBackend($cache_backend, 'rift_source_plugins');
  }

  /**
   * {@inheritdoc}
   */
  protected function getType() {
    return 'rift_source';
  }

}
