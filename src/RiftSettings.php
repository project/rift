<?php

declare(strict_types=1);

namespace Drupal\rift;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\ImageToolkit\ImageToolkitManager;
use Symfony\Component\Yaml\Yaml;

/**
 * Provides a RIFT Settings manager service.
 *
 * RIFT has a complex settings system with many inter-connected dependencies.
 * Using this service, we try to keep the integrity in check.
 */
final class RiftSettings {

  /**
   * Template Directory.
   *
   * String.
   */
  protected string $templatePath;

  public function __construct(
    private readonly ImageToolkitManager $imageToolkitManager,
    private readonly UuidInterface $uuid,
    private readonly ModuleExtensionList $extensionList,
    protected $configFactory,
    private readonly RiftSourceManager $riftSource,
    private readonly RiftMediaSourceManager $riftMediaSource,
  ) {
    $this->templatePath = $this->extensionList->getPath('rift') . '/assets/templates/';
  }

  /**
   * Get RIFT Media Source Plugins.
   */
  public function getAvailableMediaSourcePlugins(): array {
    $plugins = $this->riftMediaSource->getDefinitions();
    $options = [];
    foreach ($plugins as $plugin) {
      $id = $plugin['id'];
      $options[$id] = $plugin['label'];
    }
    return $options;
  }

  /**
   * Get RIFT Source Plugins.
   */
  public function getAvailableSourcePlugins(): array {
    $plugins = $this->riftSource->getDefinitions();
    $options = [];
    foreach ($plugins as $plugin) {
      $id = $plugin['id'];
      $options[$id] = $plugin['label'];
    }
    return $options;
  }

  /**
   * Get available extension suggestions.
   *
   * @return string[]
   *   List of extension.
   */
  public function getAvailableExtensions(): array {
    $extensions = $this->imageToolkitManager->getDefaultToolkit()->getSupportedExtensions();
    $options = array_combine(
      $extensions,
      array_map('mb_strtoupper', $extensions)
    );
    $options['nop'] = "Use original format";
    return $options;
  }

  /**
   * Get RIFT settings.
   */
  public function getSettings(): array {
    $settings = $this->configFactory->get('rift.settings');
    return $settings->getRawData();
  }

  /**
   * Save RIFT settings.
   */
  public function saveSettings($input): void {
    if (!empty($input['aspect_ratios'])) {
      $this->generateAspectRatiosConfigurations($input['aspect_ratios']);
    }
    if (!empty($input['config']['quality'])) {
      $this->generateQualityConfigurations($input['config']['quality']);
    }
    if (!empty($input['config']['formats'])) {
      $this->generateExtensionConfigurations($input['config']['formats']);
    }
    $this->generateConfig('image.style.nop.yml', 'image.style.nop', []);
    $this->generateConfig('crop.type.focal_point.yml', 'crop.type.focal_point', []);

    $settings = $this->configFactory->getEditable('rift.settings');
    $temp_settings = $settings->getRawData();
    $map = ['source', 'media_source', 'aspect_ratios', 'config', 'view_modes'];
    foreach ($map as $key) {
      if (isset($input[$key])) {
        $temp_settings[$key] = $input[$key];
      }
    }
    $settings->setData($temp_settings);
    $settings->save(FALSE);
  }

  /**
   * Generate extension configurations.
   *
   * @param array $extensions
   *   Array of extensions to be generated.
   */
  protected function generateExtensionConfigurations($extensions) {
    $supported_extensions = $this->imageToolkitManager->getDefaultToolkit()->getSupportedExtensions();
    foreach ($extensions as $extension) {
      if (!in_array($extension, $supported_extensions)) {
        continue;
      }
      $context = [
        '%extension%' => $extension,
      ];
      $this->generateConfig('image.style.image_convert.yml', 'image.style.' . $extension, $context);
    }
  }

  /**
   * Generate quality configurations.
   *
   * @param string[] $quality_items
   *   Array of quality configuration to be generated.
   */
  protected function generateQualityConfigurations($quality_items) {
    foreach ($quality_items as $quality) {
      $context = [
        '%quality%' => $quality,
      ];
      $this->generateConfig('image.style.image_style_quality.yml', 'image.style.' . $quality, $context);
    }
  }

  /**
   * Generate aspect ratio configurations.
   *
   * @param string[] $aspect_ratios
   *   Array of aspect ratios to be generated.
   */
  protected function generateAspectRatiosConfigurations($aspect_ratios) {
    foreach ($aspect_ratios as $aspect_ratio) {
      $context = [
        '%aspect_ratio%' => $aspect_ratio,
      ];
      $this->generateConfig('crop.type.aspect_ratio.yml', 'crop.type.' . $aspect_ratio, $context);
      $this->generateConfig('image.style.crop_crop.yml', 'image.style.' . $aspect_ratio, $context);
    }
  }

  /**
   * Generate configuration from template.
   *
   * @param string $template
   *   The template to load for generating configuration.
   * @param string $config_id
   *   The config id.
   * @param array $context
   *   Variables to be used for replacing the values in the template.
   *   The variable key must be of the format "%variable_name%".
   * @param bool $replace_existing_config
   *   Controls if existing configuration should be replaced or not.
   */
  protected function generateConfig(string $template, string $config_id, array $context, bool $replace_existing_config = FALSE) {
    $config_path = $this->templatePath . $template;
    $content = file_get_contents($config_path);
    for ($ndx = 1; $ndx <= 10; $ndx++) {
      $context['%uuid_' . $ndx . '%'] = $this->uuid->generate();
    }
    $transformed_content = str_replace(array_keys($context), $context, $content);
    $data = Yaml::parse($transformed_content);
    $config = $this
      ->configFactory
      ->getEditable($config_id)
      ->setData($data);
    if ($config->isNew()) {
      $config
        ->save(TRUE);
    }
    else {
      if ($replace_existing_config) {
        $config
          ->save(TRUE);
      }
    }
  }

}
