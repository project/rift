<?php

namespace Drupal\rift\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\ConfigTarget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\rift\RiftMediaSourceManager;
use Drupal\rift\RiftSourceManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Configure Responsive Image Twig Filter settings for this site.
 */
class RiftSettingsForm extends ConfigFormBase {

  /**
   * The rift source manager service.
   *
   * @var \Drupal\rift\RiftSourceManager
   */
  protected readonly RiftSourceManager $riftSource;

  /**
   * The rift media source manager service.
   *
   * @var \Drupal\rift\RiftMediaSourceManager
   */
  protected readonly RiftMediaSourceManager $riftMediaSource;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->riftSource = $container->get('plugin.manager.rift_source');
    $instance->riftMediaSource = $container->get('plugin.manager.rift_media_source');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'rift_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['rift.settings'];
  }

  /**
   * Get RIFT Media Source Plugins.
   */
  protected function getMediaSourcePlugins(): array {
    return $this->riftMediaSource->getDefinitions();
  }

  /**
   * Get RIFT Source Plugins.
   */
  protected function getSourcePlugins(): array {
    return $this->riftSource->getDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $media_sources = $this->getMediaSourcePlugins();
    $options = [];
    foreach ($media_sources as $media_source) {
      $id = $media_source['id'];
      $options[$id] = $media_source['label'];
    }
    $form['media_source'] = [
      '#title' => $this->t('RIFT Media Source Plugin (Input processor)'),
      '#type' => 'select',
      '#options' => $options,
      '#multiple' => FALSE,
      '#required' => TRUE,
      '#description' => $this->t('Input Plugin that should be used by RIFT formatter.'),
      '#default_value' => $this->config('rift.settings')->get('media_source'),
    ];

    $sources = $this->getSourcePlugins();
    $options = [];
    foreach ($sources as $source) {
      $id = $source['id'];
      $options[$id] = $source['label'];
    }
    $form['source'] = [
      '#title' => $this->t('RIFT Source Plugin (Output Processor)'),
      '#type' => 'select',
      '#options' => $options,
      '#multiple' => FALSE,
      '#required' => TRUE,
      '#description' => $this->t('The plugin that would be used to output the image urls used for &lt;source&gt; tag.'),
      '#default_value' => $this->config('rift.settings')->get('source'),
    ];

    $form['config'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Default Config'),
      '#attributes' => ['data-yaml-editor' => 'true'],
      '#default_value' => $this->yamlDump($this->config('rift.settings')->get('config')),
      '#config_target' => new ConfigTarget(
        'rift.settings',
        'config',
        // Converts config value to a form value.
        fn($value) => $this->yamlDump($value),
        // Converts form value to a config value.
        fn($value) => $this->yamlParse($value),
      ),
    ];
    $form['view_modes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('View Modes'),
      '#attributes' => ['data-yaml-editor' => 'true'],
      '#default_value' => $this->yamlDump($this->config('rift.settings')->get('view_modes')),
      '#config_target' => new ConfigTarget(
        'rift.settings',
        'view_modes',
        // Converts config value to a form value.
        fn($value) => $this->yamlDump($value),
        // Converts form value to a config value.
        fn($value) => $this->yamlParse($value),
      ),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('rift.settings')
      ->set('config', Yaml::parse($form_state->getValue('config')))
      ->set('view_modes', Yaml::parse($form_state->getValue('view_modes')))
      ->set('media_source', Yaml::parse($form_state->getValue('media_source')))
      ->set('source', Yaml::parse($form_state->getValue('source')))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Dumps a PHP value to a YAML string.
   *
   * @param mixed $data
   *   The PHP value.
   *
   * @return string
   *   Yaml file content.
   */
  protected function yamlDump($data) {
    return Yaml::dump($data, 4);
  }

  /**
   * Parses YAML into a PHP value.
   *
   * @param string $data
   *   string containing YAML.
   *
   * @return mixed
   *   The PHP value.
   */
  protected function yamlParse($data) {
    return Yaml::parse($data);
  }

}
