<?php

namespace Drupal\rift\Plugin\RiftMediaSource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\media\MediaInterface;
use Drupal\rift\Attribute\RiftMediaSource;
use Drupal\rift\DTO\SourceTransformConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Use Core "image" media as media source.
 */
#[RiftMediaSource(
  id: 'multiple_image',
  label: new TranslatableMarkup('Media with multiple image'),
  description: new TranslatableMarkup('Use Media of type "image" as source plugin. Supports optional breakpoint specific images as source image for a breakpoint.')
)]
class MultipleImage extends ImageSourceBase implements ContainerFactoryPluginInterface {

  /**
   * The crop storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $cropTypeStorage;

  /**
   * The constructor.
   */
  public function __construct(
    private readonly EntityTypeManagerInterface $entityTypeManager,
  ) {
    $this->cropTypeStorage = $entityTypeManager->getStorage('crop_type');
  }

  /**
   * {@inheritdoc}
   */
  #[\Override] public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new self(
      $container->get('entity_type.manager'),
    );
  }

  /**
   * Detect Media field name.
   *
   * @param \Drupal\media\MediaInterface $media
   *   A render array element.
   * @param \Drupal\rift\DTO\SourceTransformConfig|null $transformConfig
   *   The transform config.
   *
   * @return string
   *   The Field Name.
   */
  protected function getImageFieldName(MediaInterface $media, ?SourceTransformConfig $transformConfig = NULL): string {
    $source = $media->getSource();
    $default_field = $source->getConfiguration()["source_field"];
    if ($transformConfig) {
      $screen = $transformConfig->screen;
      $candidates = [
        $default_field . '_' . $screen,
      ];
      foreach ($candidates as $field_name) {
        // @todo Making mapping of candiate source field configurable.
        if ($media->hasField($field_name) && !$media->get($field_name)->isEmpty()) {
          return $field_name;
        }
      }
    }
    return $default_field;
  }

}
