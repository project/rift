<?php

namespace Drupal\rift\Plugin\RiftMediaSource;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\crop\Entity\Crop;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\media\MediaInterface;
use Drupal\rift\DTO\SourceTransformConfig;
use Drupal\rift\Html\ImgElement;
use Drupal\rift\RiftMediaSourceInterface;

/**
 * Use Core "image" media as media source.
 */
abstract class ImageSourceBase implements RiftMediaSourceInterface {
  /**
   * The crop storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $cropTypeStorage;

  /**
   * {@inheritdoc}
   */
  public function validateMedia($media): bool {
    if ($media instanceof MediaInterface) {
      $image = $this->getImageData($media);
      return !empty($image->getSrc());
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getImageData(?MediaInterface $media = NULL, ?SourceTransformConfig $transformConfig = NULL): ImgElement {
    $image = new ImgElement();
    if ($file = $this->getImageFileEntity($media, $transformConfig)) {
      $image_field = $this->getImageField($media, $transformConfig)[0];
      $values = $image_field->getValue();
      $image
        ->setTitle($values['title'])
        ->setAlt($values['alt'])
        ->setWidth($values['width'])
        ->setHeight($values['height']);
      if ($file instanceof File) {
        $uri = $file->getFileUri();
        $image->setSrc($uri);
      }
    }
    return $image;
  }

  /**
   * {@inheritdoc}
   */
  public function generateImageBoundaryStyle(MediaInterface $media, SourceTransformConfig $transformConfig): string {
    $transform_width = $transformConfig->width;
    $aspect_ratio_style = $transformConfig->aspectRatioStyle;
    if ($aspect_ratio_style && $this->detectIfCropExists($aspect_ratio_style, $media)) {
      return $aspect_ratio_style . '-' . $transform_width . 'w';
    }
    else {
      assert(!empty($aspect_ratio_style), 'Aspect ratio must not be empty.');
      [$arw, $arh] = explode('x', $aspect_ratio_style);
      $arh_int = intval($arh);
      $arw_int = intval($arw);
      assert(!empty($arh_int), 'Invalid height found for aspect ratio.');
      assert(!empty($arw_int), 'Invalid width found for aspect ratio.');
      try {
        $aspect_ratio = intval($arw) / intval($arh);
        $height = intval($transform_width / $aspect_ratio);
      }
      catch (\DivisionByZeroError $exception) {
        // Use a 4:3 aspect ratio as a failsafe.
        $height = intval($transform_width / (4 / 3));
      }
      return $transform_width . 'w' . $height . 'h';
    }
  }

  /**
   * Get Image field from given render array element.
   *
   * @param \Drupal\media\MediaInterface $media
   *   A render array element.
   * @param \Drupal\rift\DTO\SourceTransformConfig|null $transformConfig
   *   The transform config.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   Media entity if present, otherwise null.
   */
  protected function getImageFileEntity(MediaInterface $media, ?SourceTransformConfig $transformConfig = NULL): ?EntityInterface {
    $field_items = $this->getImageField($media, $transformConfig);
    if ($field_items instanceof EntityReferenceFieldItemListInterface) {
      return $field_items->referencedEntities()[0] ?? NULL;
    }
    return NULL;
  }

  /**
   * Get Image field from given render array element.
   *
   * @param \Drupal\media\MediaInterface $media
   *   A render array element.
   * @param \Drupal\rift\DTO\SourceTransformConfig|null $transformConfig
   *   The transform config.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface
   *   Media entity if present, otherwise null.
   */
  protected function getImageField(MediaInterface $media, ?SourceTransformConfig $transformConfig = NULL): FieldItemListInterface {
    $field_name = $this->getImageFieldName($media, $transformConfig);
    return $media->get($field_name);
  }

  /**
   * Detect Media field name.
   *
   * @param \Drupal\media\MediaInterface $media
   *   A render array element.
   * @param \Drupal\rift\DTO\SourceTransformConfig|null $transformConfig
   *   The transform config.
   *
   * @return string
   *   The field name pointing to the image field.
   */
  protected function getImageFieldName(MediaInterface $media, ?SourceTransformConfig $transformConfig = NULL): string {
    $source = $media->getSource();
    return $source->getConfiguration()["source_field"];
  }

  /**
   * Detect if a manual crop is applied on the image.
   */
  protected function detectIfCropExists(string $aspect_ratio_style, ?MediaInterface $media = NULL): bool {
    $crop_type = $this->cropTypeStorage->load($aspect_ratio_style);
    if (empty($crop_type)) {
      return FALSE;
    }
    $field_name = $this->getImageFieldName($media);
    $file = $media->get($field_name)->entity;
    if ($file instanceof FileInterface) {
      $file_uri = $file->getFileUri();
      return Crop::cropExists($file_uri, $aspect_ratio_style);
    }
    return FALSE;
  }

}
