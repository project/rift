<?php

namespace Drupal\rift\Plugin\RiftMediaSource;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\media\MediaInterface;
use Drupal\rift\Attribute\RiftMediaSource;
use Drupal\rift\DTO\SourceTransformConfig;
use Drupal\rift\Html\ImgElement;
use Drupal\rift\RiftMediaSourceInterface;

/**
 * Use Core "image" media as media source.
 */
#[RiftMediaSource(
  id: 'placeholder',
  label: new TranslatableMarkup('Placeholder'),
  description: new TranslatableMarkup('Override all images with a placeholder image from external source.')
)]
class Placeholder implements RiftMediaSourceInterface {

  /**
   * {@inheritdoc}
   */
  public function validateMedia($media = NULL): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getImageData(?MediaInterface $media = NULL, ?SourceTransformConfig $transformConfig = NULL): ImgElement {
    $image = new ImgElement();
    $image
      ->setTitle('Placeholder')
      ->setAlt('placeholder image used for demo purpose');
    return $image;
  }

  /**
   * {@inheritdoc}
   */
  public function generateImageBoundaryStyle(?MediaInterface $media = NULL, ?SourceTransformConfig $transformConfig = NULL): string {
    $height = intval($transformConfig->width / (4 / 3));
    return $transformConfig->width . 'w' . $height . 'h';
  }

}
