<?php

namespace Drupal\rift\Plugin\RiftMediaSource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\rift\Attribute\RiftMediaSource;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Use Core "image" media as media source.
 */
#[RiftMediaSource(
  id: 'image',
  label: new TranslatableMarkup('Media Image'),
  description: new TranslatableMarkup('Use Media of type "image" as source plugin.')
)]
class Image extends ImageSourceBase implements ContainerFactoryPluginInterface {

  /**
   * The constructor.
   */
  public function __construct(
    private readonly EntityTypeManagerInterface $entityTypeManager,
  ) {
    $this->cropTypeStorage = $entityTypeManager->getStorage('crop_type');
  }

  /**
   * {@inheritdoc}
   */
  #[\Override] public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new self(
      $container->get('entity_type.manager'),
    );
  }

}
