<?php

namespace Drupal\rift\Plugin\Field\FieldFormatter;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\media\Entity\Media;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'RiftPicture' formatter.
 *
 * @FieldFormatter(
 *   id = "rift_media_entity_reference_picture_with_fallback",
 *   label = @Translation("Rift Media Picture (with fallback)"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class RiftMediaEntityReferencePictureWithFallbackFormatter extends EntityReferenceEntityFormatter {

  /**
   * The picture view modes manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $riftPictureViewModesManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs an RiftMediaEntityReferencePictureFormatter instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $rift_picture_view_modes_manager
   *   The picture view modes manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    LoggerChannelFactoryInterface $logger_factory,
    EntityTypeManagerInterface $entity_type_manager,
    EntityDisplayRepositoryInterface $entity_display_repository,
    PluginManagerInterface $rift_picture_view_modes_manager,
    ConfigFactoryInterface $config_factory,
  ) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $label,
      $view_mode,
      $third_party_settings,
      $logger_factory,
      $entity_type_manager,
      $entity_display_repository
    );
    $this->riftPictureViewModesManager = $rift_picture_view_modes_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('logger.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository'),
      $container->get('plugin.manager.rift_picture_view_modes'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements['rift_view_mode'] = [
      '#type' => 'select',
      '#options' => $this->getAvailableViewModes(),
      '#title' => $this->t('Rift View mode'),
      '#default_value' => $this->getSetting('view_mode'),
      '#required' => TRUE,
    ];
    $elements['view_mode'] = [
      '#type' => 'select',
      '#options' => $this->entityDisplayRepository->getViewModeOptions($this->getFieldSetting('target_type')),
      '#title' => $this->t('View Mode (Fallback)'),
      '#default_value' => $this->getSetting('view_mode'),
      '#required' => TRUE,
      '#description' => $this->t('Set setting is identical to the "View Mode" setting in Entity Reference formmater. \
      In case the target media bundle is not compatible with RIFT (E.g. Video, SVG, etc.) then the entity would be rendered\
      using the selected view mode.'),
    ];

    return $elements;
  }

  /**
   * Get list of view modes.
   *
   * @return array
   *   The picture view modes as select list options.
   */
  protected function getAvailableViewModes(): array {
    $definitions = $this->riftPictureViewModesManager->getDefinitions();
    $options = [
      '' => $this->t('-Select-'),
    ];
    if (!empty($definitions)) {
      foreach ($definitions as $id => $definition) {
        $options[$id] = $definition['label'] ?? $id;
      }
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'rift_view_mode' => '',
      'view_mode' => 'default',
      'link' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    $view_mode = $this->getSetting('rift_view_mode');
    if (!($items instanceof EntityReferenceFieldItemListInterface)) {
      return $elements;
    }
    if ($items) {
      $definitions = $this->riftPictureViewModesManager->getDefinitions();
    }
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {
      if ($entity instanceof Media) {
        if ($entity->getSource()->getPluginId() !== 'image') {
          continue;
        }
      }
      $elements[$delta] = [
        '#type' => 'inline_template',
        '#template' => '{{ media|rift_picture(config) }}',
        '#context' => [
          'media' => $entity,
          'config' => $definitions[$view_mode] ?? [],
        ],
        '#cache' => [
          'tags' => Cache::mergeTags(
            $entity->getCacheTags(),
            $this->configFactory->get('rift.settings')->getCacheTags()
          ),
          'contexts' => $entity->getCacheContexts(),
          'max-age' => $entity->getCacheMaxAge(),
          'keys' => [
            'entity_view',
            $entity->getEntityTypeId(),
            $entity->id(),
            $view_mode,
          ],
        ],
      ];
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    if (parent::isApplicable($field_definition) &&
      $field_definition->getType() == 'entity_reference' &&
      $field_definition->getSetting('target_type') == 'media') {
      return TRUE;
    }
    return FALSE;
  }

}
