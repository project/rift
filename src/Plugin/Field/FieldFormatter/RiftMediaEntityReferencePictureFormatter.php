<?php

namespace Drupal\rift\Plugin\Field\FieldFormatter;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\media\Entity\MediaType;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'RiftPicture' formatter.
 *
 * @FieldFormatter(
 *   id = "rift_media_entity_reference_picture",
 *   label = @Translation("Rift Media Picture"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class RiftMediaEntityReferencePictureFormatter extends EntityReferenceEntityFormatter {

  /**
   * The picture view modes manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $riftPictureViewModesManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs an RiftMediaEntityReferencePictureFormatter instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $rift_picture_view_modes_manager
   *   The picture view modes manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    LoggerChannelFactoryInterface $logger_factory,
    EntityTypeManagerInterface $entity_type_manager,
    EntityDisplayRepositoryInterface $entity_display_repository,
    PluginManagerInterface $rift_picture_view_modes_manager,
    ConfigFactoryInterface $config_factory,
  ) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $label,
      $view_mode,
      $third_party_settings,
      $logger_factory,
      $entity_type_manager,
      $entity_display_repository
    );
    $this->riftPictureViewModesManager = $rift_picture_view_modes_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('logger.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository'),
      $container->get('plugin.manager.rift_picture_view_modes'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements['view_mode'] = [
      '#type' => 'select',
      // @todo Convert Rift Media options to plugins/settings.
      '#options' => $this->getAvailableViewModes(),
      '#title' => $this->t('Rift View mode'),
      '#default_value' => $this->getSetting('view_mode'),
      '#required' => TRUE,
    ];
    return $elements;
  }

  /**
   * Get list of view modes.
   *
   * @return array
   *   The picture view modes as select list options.
   */
  protected function getAvailableViewModes(): array {
    $definitions = $this->riftPictureViewModesManager->getDefinitions();
    $options = [
      '' => $this->t('-Select-'),
    ];
    if (!empty($definitions)) {
      foreach ($definitions as $id => $definition) {
        $options[$id] = $definition['label'] ?? $id;
      }
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $view_mode = $this->getSetting('view_mode');
    if (!($items instanceof EntityReferenceFieldItemListInterface)) {
      return $element;
    }
    $definitions = $this->riftPictureViewModesManager->getDefinitions();
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {
      $element[$delta] = [
        '#type' => 'inline_template',
        '#template' => '{{ media|rift_picture(config) }}',
        '#context' => [
          'media' => $entity,
          'config' => $definitions[$view_mode] ?? [],
        ],
        '#cache' => [
          'tags' => Cache::mergeTags(
            $entity->getCacheTags(),
            $this->configFactory->get('rift.settings')->getCacheTags()
          ),
          'contexts' => $entity->getCacheContexts(),
          'max-age' => $entity->getCacheMaxAge(),
          'keys' => [
            'entity_view',
            $entity->getEntityTypeId(),
            $entity->id(),
            $view_mode,
          ],
        ],
      ];
    }
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    if (!parent::isApplicable($field_definition)
      || $field_definition->getType() !== 'entity_reference'
      || $field_definition->getSetting('target_type') !== 'media') {
      return FALSE;
    }
    $handler_settings = $field_definition->getSetting('handler_settings');
    if (!isset($handler_settings['target_bundles'])) {
      return FALSE;
    }
    $target_bundles_image = TRUE;
    foreach ($handler_settings['target_bundles'] as $target_bundle) {
      $media_type = MediaType::load($target_bundle);
      if ($media_type->getSource()->getPluginId() !== 'image') {
        $target_bundles_image = FALSE;
        break;
      }
    }
    return $target_bundles_image;
  }

}
