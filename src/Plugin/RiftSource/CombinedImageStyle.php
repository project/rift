<?php

namespace Drupal\rift\Plugin\RiftSource;

use Drupal\rift\Entity\CombinedImageStyle as CombinedImageStyleService;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\image\Entity\ImageStyle;
use Drupal\image\ImageEffectManager;
use Drupal\image\ImageStyleInterface;
use Drupal\rift\Attribute\RiftSource;
use Drupal\rift\DTO\PictureConfig;
use Drupal\rift\RiftSourceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Use "combined_image_style" module to generate responsive images.
 */
#[RiftSource(
  id: 'combined_image_style',
  label: new TranslatableMarkup('Combined Image Style'),
  description: new TranslatableMarkup('Use "combined_image_style" contrib module to generate responsive images.')
)]
class CombinedImageStyle implements RiftSourceInterface, ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function generate(array $input_styles, ?string $uri): string {
    $style = implode('-', array_filter($input_styles));
    $styles = explode('-', $style);
    $this->processImageStyles($styles);
    $combinedImageStyle = (new CombinedImageStyleService())
      ->setSourceUri($uri)
      ->setImageStyles($styles);
    return $combinedImageStyle->buildCombinedUrl();
  }

  /**
   * Stores overall configuration.
   *
   * @var \Drupal\rift\DTO\PictureConfig
   */
  protected PictureConfig $pictureConfig;

  /**
   * The constructor.
   */
  public function __construct(
    private readonly ModuleHandlerInterface $moduleHandler,
    private readonly ImageEffectManager $pluginManagerImageEffect,
    private readonly EntityTypeManager $entityTypeManager,
  ) {
    $this->pictureConfig = new PictureConfig();
  }

  /**
   * {@inheritdoc}
   */
  #[\Override] public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new self(
      $container->get('module_handler'),
      $container->get('plugin.manager.image.effect'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * Get Image style from given source.
   */
  protected function getImageStyleFromSource($source): ?ImageStyleInterface {
    $style_id = $this->getImageStyleIdFromSource($source);
    $image_style = $this->entityTypeManager
      ->getStorage('image_style')
      ->load($style_id);
    if (!($image_style instanceof ImageStyleInterface)) {
      $image_style = $this->generateImageStyle($source, FALSE);
    }
    return $image_style ?: NULL;
  }

  /**
   * Generates an image style based on source definition.
   *
   * @param string $source
   *   The source definition.
   * @param bool $check_width
   *   Validate against width. Throws error when width can't be determined.
   *
   * @return \Drupal\image\ImageStyleInterface|null
   *   Image style.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function generateImageStyle(string $source, bool $check_width = TRUE): ?ImageStyleInterface {
    $id = $this->getImageStyleIdFromSource($source);
    $height = $this->getHeightFromSource($source);
    $width = $this->getWidthFromSource($source);
    if ($check_width && empty($width)) {
      trigger_error('Width cannot be empty', E_USER_WARNING);
      return NULL;
    }
    /** @var \Drupal\image\ImageStyleInterface $style */
    $style = ImageStyle::create(['name' => $id, 'label' => $id]);
    $type = 'image_scale';
    if ($height) {
      if ($this->moduleHandler->moduleExists('focal_point')) {
        $type = 'focal_point_scale_and_crop';
      }
      else {
        $type = 'image_scale_and_crop';
      }
    }
    $configuration = [
      'id' => $type,
      'uuid' => NULL,
      'weight' => 1,
      'data' => [
        'upscale' => FALSE,
        'width' => $width,
        'height' => $height,
      ],
    ];
    $effect = $this->pluginManagerImageEffect->createInstance($configuration['id'], $configuration);
    $style->addImageEffect($effect->getConfiguration());

    $extension = $this->getFileExtensionFromSource($source);
    if ($extension) {
      $configuration = [
        'id' => 'image_convert',
        'uuid' => NULL,
        'weight' => 2,
        'data' => [
          'extension' => $extension,
        ],
      ];
      $effect = $this->pluginManagerImageEffect->createInstance($configuration['id'], $configuration);
      $style->addImageEffect($effect->getConfiguration());
    }
    $style->save();
    return $style;
  }

  /**
   * Detect file extension from given source.
   *
   * @param string $source
   *   The source configuration.
   *
   * @return mixed|string|null
   *   The file extension if present.
   */
  protected function getFileExtensionFromSource($source) {
    return pathinfo($source)['extension'] ?? NULL;
  }

  /**
   * Detect height settings from source config.
   *
   * @param string $source
   *   The source configuration.
   *
   * @return mixed|null
   *   Height if present, NULL otherwise.
   */
  protected function getHeightFromSource($source) {
    preg_match('/(\d+)h/', $source, $matches, PREG_OFFSET_CAPTURE, 0);
    return empty($matches[1][0]) ? NULL : $matches[1][0];
  }

  /**
   * Detect width settings from source config.
   *
   * @param string $source
   *   The source configuration.
   *
   * @return mixed|null
   *   Width if present, NULL otherwise.
   */
  protected function getWidthFromSource($source) {
    preg_match('/(\d+)w/', $source, $matches, PREG_OFFSET_CAPTURE, 0);
    return empty($matches[1][0]) ? NULL : $matches[1][0];
  }

  /**
   * Generate Style ID from given source config.
   *
   * @param string $source
   *   The source configuration.
   *
   * @return string
   *   Machine name for style.
   */
  protected function getImageStyleIdFromSource($source) {
    $prefix = '';
    $name = strtolower($source);
    $name = preg_replace('/[^a-z0-9_]+/', '_', $name);
    $name = preg_replace('/_+/', '_', $name);
    return $prefix . $name;
  }

  /**
   * Process image styles identified from dynamic configuration.
   *
   * @param string[] $styles
   *   List of styles.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function processImageStyles(array $styles) {
    foreach ($styles as $style) {
      $height = $this->getHeightFromSource($style);
      $width = $this->getWidthFromSource($style);
      if ($height || $width) {
        $this->getImageStyleFromSource($style);
      }
    }
  }

}
