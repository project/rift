<?php

namespace Drupal\rift\Plugin\RiftSource;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\rift\Attribute\RiftSource;
use Drupal\rift\RiftSourceInterface;

/**
 * Use "placehold.co" service to generate placeholder images.
 */
#[RiftSource(
  id: 'placeholdco',
  label: new TranslatableMarkup('placehold.co'),
  description: new TranslatableMarkup('Use "https://placehold.co/" external service generate responsive placeholder images.')
)]
class Placeholdco implements RiftSourceInterface {

  /**
   * {@inheritdoc}
   */
  public function generate(array $input_styles, ?string $uri): string {
    $style = implode('-', array_filter($input_styles));
    $height = $this->getHeightFromSource($style);
    $width = $this->getWidthFromSource($style);
    return 'https://placehold.co/' . $width . 'x' . $height . '?text=' . $style;
  }

  /**
   * Detect height settings from source config.
   *
   * @param string $source
   *   The source configuration.
   *
   * @return mixed|null
   *   Height if present, NULL otherwise.
   */
  protected function getHeightFromSource($source) {
    preg_match('/(\d+)h/', $source, $matches, PREG_OFFSET_CAPTURE, 0);
    return empty($matches[1][0]) ? NULL : $matches[1][0];
  }

  /**
   * Detect width settings from source config.
   *
   * @param string $source
   *   The source configuration.
   *
   * @return mixed|null
   *   Width if present, NULL otherwise.
   */
  protected function getWidthFromSource($source) {
    preg_match('/(\d+)w/', $source, $matches, PREG_OFFSET_CAPTURE, 0);
    return empty($matches[1][0]) ? NULL : $matches[1][0];
  }

}
