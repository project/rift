<?php

namespace Drupal\rift\Plugin\RiftSource;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\rift\Attribute\RiftSource;
use Drupal\rift\RiftSourceInterface;

/**
 * Use "dummyimage.com" service to generate placeholder images.
 */
#[RiftSource(
  id: 'dummyimage',
  label: new TranslatableMarkup('dummyimage.com'),
  description: new TranslatableMarkup('Use "https://dummyimage.com/" external service generate responsive placeholder images.')
)]
class DummyImage implements RiftSourceInterface {

  /**
   * {@inheritdoc}
   */
  public function generate(array $input_styles, ?string $uri): string {
    $style = implode('-', array_filter($input_styles));
    $height = $this->getHeightFromSource($style);
    $width = $this->getWidthFromSource($style);
    return 'https://dummyimage.com/' . $width . 'x' . $height . '/eee/222?text=' . $style;
  }

  /**
   * Detect height settings from source config.
   *
   * @param string $source
   *   The source configuration.
   *
   * @return mixed|null
   *   Height if present, NULL otherwise.
   */
  protected function getHeightFromSource($source) {
    preg_match('/(\d+)h/', $source, $matches, PREG_OFFSET_CAPTURE, 0);
    return empty($matches[1][0]) ? NULL : $matches[1][0];
  }

  /**
   * Detect width settings from source config.
   *
   * @param string $source
   *   The source configuration.
   *
   * @return mixed|null
   *   Width if present, NULL otherwise.
   */
  protected function getWidthFromSource($source) {
    preg_match('/(\d+)w/', $source, $matches, PREG_OFFSET_CAPTURE, 0);
    return empty($matches[1][0]) ? NULL : $matches[1][0];
  }

}
