<?php

namespace Drupal\rift;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Responsive image/picture filters for Twig.
 */
class Rift extends AbstractExtension {

  /**
   * The module handler to invoke alter hooks.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The theme manager to invoke alter hooks.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $themeManager;

  /**
   * Responsive Picture generator service.
   *
   * @var \Drupal\rift\RiftPicture
   */
  protected $riftPicture;

  /**
   * Constructs the TwigTweakExtension object.
   */
  public function __construct(
    ModuleHandlerInterface $module_handler,
    ThemeManagerInterface $theme_manager,
    RiftPicture $rift_picture,
  ) {
    $this->moduleHandler = $module_handler;
    $this->themeManager = $theme_manager;
    $this->riftPicture = $rift_picture;
  }

  /**
   * {@inheritdoc}
   */
  public function getFilters(): array {
    $filters = [
      new TwigFilter('rift_picture', $this->responsivePicture(...)),
    ];
    $this->moduleHandler->alter('rift', $filters);
    $this->themeManager->alter('rift', $filters);
    return $filters;
  }

  /**
   * Responsive Picture twig filter.
   *
   * @param \Drupal\media\MediaInterface|null $media
   *   Render array element.
   * @param array $config
   *   Configuration for generating srcset.
   *   <code>
   *   node.field_common_image.0.entity|rift_picture({
   *   sizes: "120w 320w 320w 1024w 1024w",
   *   aspect_ratios: "1x1 16x9 1x1 1x1 1x1",
   *   quality: {
   *   1x: "80 80 80 80 80",
   *   2x: "40 40 40 40 40",
   *   },
   *   types: {webp avif jpeg},
   *   })
   *   </code>.
   *
   * @return string[]
   *   Render array for responsive picture tag.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function responsivePicture($media, array $config = []) {
    return $this->riftPicture->responsivePicture($media, $config);
  }

}
