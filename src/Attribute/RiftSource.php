<?php

declare(strict_types=1);

namespace Drupal\rift\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines a RiftSource attribute for plugin discovery.
 *
 * RiftSource handles the generation of image urls in source tag.
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class RiftSource extends Plugin {

  /**
   * Constructs a RiftSource attribute.
   *
   * @param string $id
   *   The plugin ID.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|null $label
   *   (optional) The human-readable name of the RiftSource type.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|null $description
   *   (optional) A short description of the RiftSource type.
   */
  public function __construct(
    public readonly string $id,
    public readonly ?TranslatableMarkup $label = NULL,
    public readonly ?TranslatableMarkup $description = NULL,
  ) {}

}
