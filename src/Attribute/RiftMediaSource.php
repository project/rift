<?php

declare(strict_types=1);

namespace Drupal\rift\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines a RiftMediaSource attribute for plugin discovery.
 *
 * RiftMediaSource provides the source images for RIFT.
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class RiftMediaSource extends Plugin {

  /**
   * Constructs a RiftMediaSource attribute.
   *
   * @param string $id
   *   The plugin ID.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|null $label
   *   (optional) The human-readable name of the RiftMediaSource type.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|null $description
   *   (optional) A short description of the RiftMediaSource type.
   */
  public function __construct(
    public readonly string $id,
    public readonly ?TranslatableMarkup $label = NULL,
    public readonly ?TranslatableMarkup $description = NULL,
  ) {}

}
