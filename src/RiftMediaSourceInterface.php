<?php

namespace Drupal\rift;

use Drupal\media\MediaInterface;
use Drupal\rift\DTO\SourceTransformConfig;
use Drupal\rift\Html\ImgElement;

/**
 * Interface to be used by RiftMediaSource Plugins.
 */
interface RiftMediaSourceInterface {

  /**
   * Check if given media entity can be used by the Plugin.
   */
  public function validateMedia($media): bool;

  /**
   * Get Image data representing the source.
   */
  public function getImageData(?MediaInterface $media = NULL, ?SourceTransformConfig $transformConfig = NULL): ImgElement;

  /**
   * Generating boundary defining image style for a given parameters.
   */
  public function generateImageBoundaryStyle(MediaInterface $media, SourceTransformConfig $transformConfig): string;

}
