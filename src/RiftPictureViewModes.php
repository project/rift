<?php

namespace Drupal\rift;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Plugin\Discovery\ContainerDerivativeDiscoveryDecorator;
use Drupal\Core\Plugin\Discovery\YamlDiscovery;

/**
 * Rift Picture View Mode plugin.
 */
class RiftPictureViewModes extends DefaultPluginManager {

  /**
   * The constructor.
   */
  public function __construct(
    \Traversable $namespaces,
    ModuleHandlerInterface $moduleHandler,
    CacheBackendInterface $cache_backend,
    private readonly ConfigFactoryInterface $configFactory,
  ) {
    // Add more services as required.
    $this->setCacheBackend($cache_backend, 'rift_picture_view_modes', ['rift_picture_view_modes']);
    parent::__construct(FALSE, $namespaces, $moduleHandler);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDiscovery() {
    if (!isset($this->discovery)) {
      $this->discovery = new YamlDiscovery('rift_picture_view_modes', $this->moduleHandler->getModuleDirectories());
      $this->discovery = new ContainerDerivativeDiscoveryDecorator($this->discovery);
    }
    return $this->discovery;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitions() {
    $definitions = parent::getDefinitions();
    $settings = $this->configFactory->get('rift.settings');
    $view_modes = $settings->get('view_modes');
    if (!empty($view_modes)) {
      foreach ($view_modes as $id => $definition) {
        $definitions[$id] = $definition;
        $definitions[$id]['id'] = $id;
      }
    }
    return $definitions;
  }

}
