<?php

namespace Drupal\rift\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\image\Entity\ImageStyle as ImageStyleBase;

/**
 * Overridden ImageStyle Entity.
 */
class ImageStyle extends ImageStyleBase {

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    // To prevent the parent::postSave().
    $this->invalidateTagsOnSave($update);

    if ($update) {
      if (!empty($this->original) && $this->id() !== $this->original->id()) {
        // The old image style name needs flushing after a rename.
        $this->original->flush();
        // Update field settings if necessary.
        if (!$this->isSyncing()) {
          static::replaceImageStyle($this);
        }
      }
      elseif ($this->getEffects()->getConfiguration() !== $this->original->getEffects()->getConfiguration()) {
        // Flush image style only when effects configuration changed.
        $this->flush();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function flush($path = NULL) {
    foreach (CombinedImageStyle::loadCombinedBySingle($this) as $combinedImageStyle) {
      $combinedImageStyle->flush($path);
    }

    parent::flush($path);
  }

}
