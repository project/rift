<?php

namespace Drupal\rift\Controller;

use Drupal\rift\Entity\CombinedImageStyle;
use Drupal\image\Controller\ImageStyleDownloadController as ImageStyleDownloadControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Deliver combined image styles.
 *
 * RIFT module has critical dependency on "combined_image_styles" module.
 * However, until combined_image_style offers a stable release, we ship
 * the necessary code with our module.
 *
 * @see https://www.drupal.org/project/rift/issues/3495133
 */
class ImageStyleDownloadController extends ImageStyleDownloadControllerBase {

  /**
   * Generates a derivative, given a style and image path.
   *
   * After generating an image, transfer it to the requesting agent.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param string $scheme
   *   The file scheme, defaults to 'private'.
   * @param string $image_styles
   *   The image styles to deliver.
   * @param string $required_derivative_scheme
   *   The required scheme for the derivative image.
   *
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse|\Symfony\Component\HttpFoundation\Response
   *   The transferred file as response or some error response.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   *   Thrown when the file request is invalid.
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   Thrown when the user does not have access to the file.
   * @throws \Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException
   *   Thrown when the file is still being generated.
   */
  public function deliverCombined(Request $request, $scheme, $image_styles, string $required_derivative_scheme): Response {
    return $this->deliver($request, $scheme, CombinedImageStyle::fromName($image_styles), $required_derivative_scheme);
  }

}
