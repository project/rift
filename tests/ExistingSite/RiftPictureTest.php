<?php

namespace Drupal\rift\ExistingSite;

use Drupal\Core\Template\Attribute;
use Drupal\file\Entity\File;
use Drupal\Tests\TestFileCreationTrait;
use weitzman\DrupalTestTraits\Entity\MediaCreationTrait;
use weitzman\DrupalTestTraits\ExistingSiteBase;

/**
 * Test Rift Picture.
 */
class RiftPictureTest extends ExistingSiteBase {
  use MediaCreationTrait;
  use TestFileCreationTrait;


  /**
   * The sample image File entity to embed.
   *
   * @var \Drupal\media\MediaInterface
   */
  protected $media;

  /**
   * The sample image File entity to embed.
   *
   * @var \Drupal\file\FileInterface
   */
  protected $file;

  /**
   * URI of the image file being used for testing.
   *
   * @var string
   */
  protected $imageFileUri;

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();
    $this->imageFileUri = $this->getTestFiles('image')[0]->uri;
    // Create a sample host entity to embed images in.
    $this->file = File::create([
      'uri' => $this->imageFileUri,
    ]);
    $this->file
      ->save();
    $this->markEntityForCleanup($this->file);
    $this->media = $this->createMedia(
      [
        'bundle' => 'image',
        'name' => 'test_name_test_name_test_name',
        'field_media_image' => [
          'target_id' => $this->file->id(),
          'alt' => 'test_alt_test_alt_test_alt',
          'title' => 'test_title_test_title_test_title',
        ],
      ]
    );
    $this->markEntityForCleanup($this->media);
  }

  /**
   * Test case for media with empty bundle.
   */
  public function testEmptyImageInMedia() {
    $media = $this->createMedia(['bundle' => 'image']);
    $this->markEntityForCleanup($media);
    $service = \Drupal::service('rift.picture');
    $config = $this->getBaseConfig();
    $elements = $service->responsivePicture($media, $config);
    /** @var \Drupal\Core\Render\Renderer $renderer */
    $renderer = \Drupal::service('renderer');
    $string = $renderer->renderInIsolation($elements);
    $this->assertEquals('', $string);
  }

  /**
   * Test Aspect Ratio.
   */
  public function testAspectRatioConfig() {
    $media = $this->media;
    $service = \Drupal::service('rift.picture');
    $config = $this->getBaseConfig();
    $config['aspect_ratios'] = '2x3 24x10 100x185';
    $config['url_generation_strategy'] = 'dummyimage';
    $elements = $service->responsivePicture($media, $config);
    $this->assertEquals(10, count($elements['#context']['content']));
    $map = [
      0 => [
        'https://dummyimage.com/100x150/eee/222?text=100w150h-avif-80',
        'https://dummyimage.com/200x300/eee/222?text=200w300h-avif-40',
      ],
      1 => [
        'https://dummyimage.com/300x125/eee/222?text=300w125h-avif-80',
        'https://dummyimage.com/600x250/eee/222?text=600w250h-avif-40',
      ],
      2 => [
        'https://dummyimage.com/500x925/eee/222?text=500w925h-avif-80',
        'https://dummyimage.com/1000x1850/eee/222?text=1000w1850h-avif-40',
      ],
      3 => [
        'https://dummyimage.com/100x150/eee/222?text=100w150h-webp-80',
        'https://dummyimage.com/200x300/eee/222?text=200w300h-webp-40',
      ],
      4 => [
        'https://dummyimage.com/300x125/eee/222?text=300w125h-webp-80',
        'https://dummyimage.com/600x250/eee/222?text=600w250h-webp-40',
      ],
      5 => [
        'https://dummyimage.com/500x925/eee/222?text=500w925h-webp-80',
        'https://dummyimage.com/1000x1850/eee/222?text=1000w1850h-webp-40',
      ],
      6 => [
        'https://dummyimage.com/100x150/eee/222?text=100w150h-jpeg-80',
        'https://dummyimage.com/200x300/eee/222?text=200w300h-jpeg-40',
      ],
      7 => [
        'https://dummyimage.com/300x125/eee/222?text=300w125h-jpeg-80',
        'https://dummyimage.com/600x250/eee/222?text=600w250h-jpeg-40',
      ],
      8 => [
        'https://dummyimage.com/500x925/eee/222?text=500w925h-jpeg-80',
        'https://dummyimage.com/1000x1850/eee/222?text=1000w1850h-jpeg-40',
      ],
    ];
    foreach ($elements["#context"]["content"] as $ndx => $content) {
      /** @var \Drupal\Core\Template\Attribute $attributes */
      $attribute = $content['#context']['attributes'];
      if ($content['#context']['tag_name'] == "source") {
        $srcset_string = (string) $attribute->offsetGet('srcset');
        $srcset_items = explode(',', $srcset_string);
        $this->assertEquals(2, count($srcset_items));
        [$first_url] = explode(' ', $srcset_items[0]);
        [$second_url] = explode(' ', $srcset_items[1]);
        $this->assertEquals($map[$ndx][0], $first_url);
        $this->assertEquals($map[$ndx][1], $second_url);
      }
    }

  }

  /**
   * Test responsive picture.
   */
  public function testResponsivePicture() {
    $media = $this->media;
    $service = \Drupal::service('rift.picture');
    $config = $this->getBaseConfig();
    $elements = $service->responsivePicture($media, $config);
    /** @var \Drupal\Core\Render\Renderer $renderer */
    $renderer = \Drupal::service('renderer');
    $string = $renderer->renderInIsolation($elements);
    $this->assertNotEmpty($string, 'Expected image markup to be present, but empty markup returned.');

    $picture_attribute = $elements['#context']['attributes'];
    $this->assertInstanceOf(Attribute::class, $picture_attribute);
    $this->assertEquals('lazy', $picture_attribute->offsetGet('loading'));
    $this->assertEquals('', $picture_attribute->offsetGet('preload'));

    $this->assertEquals(10, count($elements['#context']['content']));
    $this->assertEquals('picture', $elements['#context']['tag_name']);
    $this->assertEquals('source', $elements['#context']['content'][0]['#context']['tag_name']);

    foreach ($elements['#context']['content'] as $position => $content) {
      $attribute = $content['#context']['attributes'];
      $this->assertInstanceOf(Attribute::class, $attribute);
      // Test correct 'tag' is used.
      switch ($position) {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
          $this->assertEquals('source', $content['#context']['tag_name']);
          break;

        case 9:
          $this->assertEquals('img', $content['#context']['tag_name'], 'Expected to find an img tag as last fallback item inside picture, but found ' . $content['#context']['tag_name']);
          break;

        default:
          $this->error('Un-expected content detected inside <picture> element.');
          break;
      }
      // Test attribute has correct 'type'.
      switch ($position) {
        case 0:
        case 1:
        case 2:
          $this->assertEquals('image/avif', $attribute->offsetGet('type'));
          break;

        case 3:
        case 4:
        case 5:
          $this->assertEquals('image/webp', $attribute->offsetGet('type'));
          break;

        case 6:
        case 7:
        case 8:
          $this->assertEquals('image/jpeg', $attribute->offsetGet('type'));
          break;

        case 9:
          $this->assertNull($attribute->offsetGet('type'));
          break;

        default:
          $this->error('Un-expected content detected inside <picture> element.');
          break;
      }
      // Test attribute has correct 'media'.
      switch ($position) {
        case 0:
        case 3:
        case 6:
          $this->assertEquals('(max-width: 1024px)', $attribute->offsetGet('media'));
          break;

        case 1:
        case 4:
        case 7:
          $this->assertEquals('(max-width: 1280px)', $attribute->offsetGet('media'));
          break;

        case 2:
        case 5:
        case 8:
          $this->assertNull($attribute->offsetGet('media'), 'Expected no media query to be present on last set of images, but found a media attribute to be present.');
          break;

        case 9:
          $this->assertNull($attribute->offsetGet('media'), 'Expected no media query to be present on fallback img element, but found a media attribute to be present.');
          break;

        default:
          $this->error('Un-expected content detected inside <picture> element.');
          break;
      }

      // Test attribute has correct 'sizes'.
      switch ($position) {
        case 0:
        case 3:
        case 6:
          $this->assertEquals('(max-width: 1024px) 100', $attribute->offsetGet('sizes'));
          break;

        case 1:
        case 4:
        case 7:
          $this->assertEquals('(max-width: 1280px) 300', $attribute->offsetGet('sizes'));
          break;

        case 2:
        case 5:
        case 8:
          $this->assertEquals('500', $attribute->offsetGet('sizes'));
          break;

        case 9:
          $this->assertNull($attribute->offsetGet('sizes'));
          break;

        default:
          $this->error('Un-expected content detected inside <picture> element.');
          break;
      }

      // Test attribute 'src' is correct for fallback img tag.
      if ($position == 9) {
        $this->assertStringContainsString('/medium-80/', $attribute->offsetGet('src'));
      }
      // Test attribute has correct 'srcset'.
      else {
        $srcset = $attribute->offsetGet('srcset');
        $srcsets = explode(',', $srcset);
        array_walk($srcsets, function (&$src) {
          [$key, $multiplier] = explode(' ', $src);
          $src = [
            'url' => $key,
            'multiplier' => $multiplier,
          ];
        });
        $this->assertEquals('1x', $srcsets[0]['multiplier']);
        $this->assertEquals('2x', $srcsets[1]['multiplier']);
        switch ($position) {
          case 0:
            $this->assertStringContainsString('/100w100h-avif-80/', $srcsets[0]['url']);
            $this->assertStringContainsString('/200w200h-avif-40/', $srcsets[1]['url']);
            break;

          case 1:
            $this->assertStringContainsString('/300w300h-avif-80/', $srcsets[0]['url']);
            $this->assertStringContainsString('/600w600h-avif-40/', $srcsets[1]['url']);
            break;

          case 2:
            $this->assertStringContainsString('/500w500h-avif-80/', $srcsets[0]['url']);
            $this->assertStringContainsString('/1000w1000h-avif-40/', $srcsets[1]['url']);
            break;

          case 3:
            $this->assertStringContainsString('/100w100h-webp-80/', $srcsets[0]['url']);
            $this->assertStringContainsString('/200w200h-webp-40/', $srcsets[1]['url']);
            break;

          case 4:
            $this->assertStringContainsString('/300w300h-webp-80/', $srcsets[0]['url']);
            $this->assertStringContainsString('/600w600h-webp-40/', $srcsets[1]['url']);
            break;

          case 5:
            $this->assertStringContainsString('/500w500h-webp-80/', $srcsets[0]['url']);
            $this->assertStringContainsString('/1000w1000h-webp-40/', $srcsets[1]['url']);
            break;

          case 6:
            $this->assertStringContainsString('/100w100h-jpeg-80/', $srcsets[0]['url']);
            $this->assertStringContainsString('/200w200h-jpeg-40/', $srcsets[1]['url']);
            break;

          case 7:
            $this->assertStringContainsString('/300w300h-jpeg-80/', $srcsets[0]['url']);
            $this->assertStringContainsString('/600w600h-jpeg-40/', $srcsets[1]['url']);
            break;

          case 8:
            $this->assertStringContainsString('/500w500h-jpeg-80/', $srcsets[0]['url']);
            $this->assertStringContainsString('/1000w1000h-jpeg-40/', $srcsets[1]['url']);
            break;

          default:
            $this->error('Un-expected content detected inside <picture> element.');
            break;
        }
      }
    }
  }

  /**
   * Test "rift_picture" Twig Filter.
   */
  public function testResponsivePictureTwigFilter() {
    $file = File::create([
      'uri' => $this->getTestFiles('image')[0]->uri,
    ]);
    $file->save();
    $media = $this->createMedia([
      'bundle' => 'image',
      'field_media_image' => [
        'target_id' => $file->id(),
        'title' => 'dummy title',
        'alt' => 'dummy alt',
      ],
    ]);
    $this->markEntityForCleanup($file);
    $this->markEntityForCleanup($media);
    $elements = [
      '#type' => 'inline_template',
      '#template' => '{{ media|rift_picture({
          sizes: "sm:100w",
          aspect_ratios: "1x2",
          url_generation_strategy: "placeholdit",
        })
      }}',
      '#context' => [
        'media' => $media,
      ],
    ];
    /** @var \Drupal\Core\Render\Renderer $renderer */
    $renderer = \Drupal::service('renderer');
    $output = $renderer->renderInIsolation($elements);
    $html = '<picture  loading="lazy" preload=""><source  width="100" height="200" type="image/webp" sizes="100w" srcset="https://place-hold.it/100x200?text=100w200h-webp-80 1x,https://place-hold.it/200x400?text=200w400h-webp-40 2x" /><source  width="100" height="200" type="image/jpeg" sizes="100w" srcset="https://place-hold.it/100x200?text=100w200h-jpeg-80 1x,https://place-hold.it/200x400?text=200w400h-jpeg-40 2x" /><img  src="https://place-hold.it/x?text=medium-80" alt="dummy alt" title="dummy title" width="40" height="20" /></picture>';
    $this->assertEquals($html, $output);

  }

  /**
   * Test responsive picture format support.
   */
  public function testResponsivePictureFormats() {
    $media = $this->media;
    $service = \Drupal::service('rift.picture');
    $config = $this->getBaseConfig();
    $config['formats'] = ['jpeg'];
    $elements = $service->responsivePicture($media, $config);
    $this->assertEquals(4, count($elements['#context']['content']));
    $config['formats'] = ['avif', 'jpeg'];
    $elements = $service->responsivePicture($media, $config);
    $this->assertEquals(7, count($elements['#context']['content']));
    $config['formats'] = ['avif', 'webp', 'jpeg'];
    $elements = $service->responsivePicture($media, $config);
    $this->assertEquals(10, count($elements['#context']['content']));
  }

  /**
   * Test responsive picture 'multipliers' support.
   */
  public function testResponsivePictureMultipliers() {
    $media = $this->media;
    $service = \Drupal::service('rift.picture');
    $config = $this->getBaseConfig();
    $config['multipliers'] = ['1x'];
    $elements = $service->responsivePicture($media, $config);
    $this->assertEquals(10, count($elements['#context']['content']));
    foreach ($elements["#context"]["content"] as $content) {
      /** @var \Drupal\Core\Template\Attribute $attributes */
      $attribute = $content['#context']['attributes'];
      if ($content['#context']['tag_name'] == "source") {
        $srcset_string = (string) $attribute->offsetGet('srcset');
        $srcset_items = explode(',', $srcset_string);
        $this->assertEquals(1, count($srcset_items));
        [, $first_multiplier] = explode(' ', $srcset_items[0]);
        $this->assertEquals('1x', $first_multiplier);
      }
    }

    $config['multipliers'] = ['1x', '2x'];
    $elements = $service->responsivePicture($media, $config);
    $this->assertEquals(10, count($elements['#context']['content']));
    foreach ($elements["#context"]["content"] as $content) {
      /** @var \Drupal\Core\Template\Attribute $attributes */
      $attribute = $content['#context']['attributes'];
      if ($content['#context']['tag_name'] == "source") {
        $srcset_string = (string) $attribute->offsetGet('srcset');
        $srcset_items = explode(',', $srcset_string);
        $this->assertEquals(2, count($srcset_items));
        [, $first_multiplier] = explode(' ', $srcset_items[0]);
        [, $second_multiplier] = explode(' ', $srcset_items[1]);
        $this->assertEquals('1x', $first_multiplier);
        $this->assertEquals('2x', $second_multiplier);
      }
    }
  }

  /**
   * Test responsive picture 'placeholdit' plugin.
   */
  public function testPlaceholditImageGenerator() {
    $media = $this->media;
    $service = \Drupal::service('rift.picture');
    $config = $this->getBaseConfig();
    $config['url_generation_strategy'] = 'placeholdit';
    $elements = $service->responsivePicture($media, $config);
    $this->assertEquals(10, count($elements['#context']['content']));
    $map = [
      0 => [
        'https://place-hold.it/100x100?text=100w100h-avif-80',
        'https://place-hold.it/200x200?text=200w200h-avif-40',
      ],
      1 => [
        'https://place-hold.it/300x300?text=300w300h-avif-80',
        'https://place-hold.it/600x600?text=600w600h-avif-40',
      ],
      2 => [
        'https://place-hold.it/500x500?text=500w500h-avif-80',
        'https://place-hold.it/1000x1000?text=1000w1000h-avif-40',
      ],
      3 => [
        'https://place-hold.it/100x100?text=100w100h-webp-80',
        'https://place-hold.it/200x200?text=200w200h-webp-40',
      ],
      4 => [
        'https://place-hold.it/300x300?text=300w300h-webp-80',
        'https://place-hold.it/600x600?text=600w600h-webp-40',
      ],
      5 => [
        'https://place-hold.it/500x500?text=500w500h-webp-80',
        'https://place-hold.it/1000x1000?text=1000w1000h-webp-40',
      ],
      6 => [
        'https://place-hold.it/100x100?text=100w100h-jpeg-80',
        'https://place-hold.it/200x200?text=200w200h-jpeg-40',
      ],
      7 => [
        'https://place-hold.it/300x300?text=300w300h-jpeg-80',
        'https://place-hold.it/600x600?text=600w600h-jpeg-40',
      ],
      8 => [
        'https://place-hold.it/500x500?text=500w500h-jpeg-80',
        'https://place-hold.it/1000x1000?text=1000w1000h-jpeg-40',
      ],
    ];
    foreach ($elements["#context"]["content"] as $ndx => $content) {
      /** @var \Drupal\Core\Template\Attribute $attributes */
      $attribute = $content['#context']['attributes'];
      if ($content['#context']['tag_name'] == "source") {
        $srcset_string = (string) $attribute->offsetGet('srcset');
        $srcset_items = explode(',', $srcset_string);
        $this->assertEquals(2, count($srcset_items));
        [$first_url] = explode(' ', $srcset_items[0]);
        [$second_url] = explode(' ', $srcset_items[1]);
        $this->assertEquals($map[$ndx][0], $first_url);
        $this->assertEquals($map[$ndx][1], $second_url);
      }
    }
  }

  /**
   * Test responsive picture 'dummyimage' plugin.
   */
  public function testDummyImage() {
    $media = $this->media;
    $service = \Drupal::service('rift.picture');
    $config = $this->getBaseConfig();
    $config['url_generation_strategy'] = 'dummyimage';
    $elements = $service->responsivePicture($media, $config);
    $this->assertEquals(10, count($elements['#context']['content']));
    $map = [
      0 => [
        'https://dummyimage.com/100x100/eee/222?text=100w100h-avif-80',
        'https://dummyimage.com/200x200/eee/222?text=200w200h-avif-40',
      ],
      1 => [
        'https://dummyimage.com/300x300/eee/222?text=300w300h-avif-80',
        'https://dummyimage.com/600x600/eee/222?text=600w600h-avif-40',
      ],
      2 => [
        'https://dummyimage.com/500x500/eee/222?text=500w500h-avif-80',
        'https://dummyimage.com/1000x1000/eee/222?text=1000w1000h-avif-40',
      ],
      3 => [
        'https://dummyimage.com/100x100/eee/222?text=100w100h-webp-80',
        'https://dummyimage.com/200x200/eee/222?text=200w200h-webp-40',
      ],
      4 => [
        'https://dummyimage.com/300x300/eee/222?text=300w300h-webp-80',
        'https://dummyimage.com/600x600/eee/222?text=600w600h-webp-40',
      ],
      5 => [
        'https://dummyimage.com/500x500/eee/222?text=500w500h-webp-80',
        'https://dummyimage.com/1000x1000/eee/222?text=1000w1000h-webp-40',
      ],
      6 => [
        'https://dummyimage.com/100x100/eee/222?text=100w100h-jpeg-80',
        'https://dummyimage.com/200x200/eee/222?text=200w200h-jpeg-40',
      ],
      7 => [
        'https://dummyimage.com/300x300/eee/222?text=300w300h-jpeg-80',
        'https://dummyimage.com/600x600/eee/222?text=600w600h-jpeg-40',
      ],
      8 => [
        'https://dummyimage.com/500x500/eee/222?text=500w500h-jpeg-80',
        'https://dummyimage.com/1000x1000/eee/222?text=1000w1000h-jpeg-40',
      ],
    ];
    foreach ($elements["#context"]["content"] as $ndx => $content) {
      /** @var \Drupal\Core\Template\Attribute $attributes */
      $attribute = $content['#context']['attributes'];
      if ($content['#context']['tag_name'] == "source") {
        $srcset_string = (string) $attribute->offsetGet('srcset');
        $srcset_items = explode(',', $srcset_string);
        $this->assertEquals(2, count($srcset_items));
        [$first_url] = explode(' ', $srcset_items[0]);
        [$second_url] = explode(' ', $srcset_items[1]);
        $this->assertEquals($map[$ndx][0], $first_url);
        $this->assertEquals($map[$ndx][1], $second_url);
      }
    }
  }

  /**
   * Get default configuration to be used for testing.
   */
  protected function getBaseConfig() {
    return [
      'sizes' => 'md:100 lg:300 xl:500',
      'aspect_ratios' => '1x1 1x1 1x1',
      'screens' => [
        'xs' => [
          'width' => 640,
          'media' => '(max-width: 640px)',
        ],
        'sm' => [
          'width' => 768,
          'media' => '(max-width: 768px)',
        ],
        'md' => [
          'width' => 1024,
          'media' => '(max-width: 1024px)',
        ],
        'lg' => [
          'width' => 1280,
          'media' => '(max-width: 1280px)',
        ],
        'xl' => [
          'width' => 1536,
          'media' => '',
        ],
      ],
      'transforms' => '',
      'quality' => [
        '1x' => 80,
        '2x' => 40,
      ],
      'formats' => [
        'avif',
        'webp',
        'jpeg',
      ],
      'attributes' => [
        'loading' => 'lazy',
        'preload' => '',
      ],
      'fallback_transform' => 'medium-80',
      'url_generation_strategy' => 'combined_image_style',
      'multipliers' => [
        '1x',
        '2x',
      ],
    ];
  }

}
